const axios = require('axios');

/*
 * This file will hold the API calls to the backend functions for creating/editing/getting
 * users from the Users DynamoDB table in AWS.
 */
export const backendURL = "https://axeqon86qk.execute-api.us-east-1.amazonaws.com/production/";

//creates/edits user data in the db
export async function setUserSettings(device_id, age_range, knows_diabetes, language){
    const user = {
        device_id: device_id,
        age_range: age_range,
        knows_diabetes: knows_diabetes,
        language: language
    };

    const response = await axios.post(backendURL + "users", user);

    console.log(JSON.stringify(response.data));
}