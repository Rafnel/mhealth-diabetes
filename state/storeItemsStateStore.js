/**
 * @name storeItemsStateStore
 * @author Andrew Case
 *
 * @overview Contains the state for store items.
 * NOTE: To add items to the store, you must add the name (in both Hindi & English)
 * and a corresponding price to this file.
 *
 */

import { observable, decorate, onBecomeObserved } from 'mobx';

export default class StoreItemsStateStore {
    owned = [];
    storeItems = ["mustache","tophat","glasses"];
    storeItemsHindi = ["मूंछ","लंबा टोप","चश्मा"]
    storeItemCosts = ["1","3","5"];
}

decorate(StoreItemsStateStore, {
    owned: observable,
    storeItems: observable,
    storeItemsHindi: observable,
    storeItemCosts: observable
});