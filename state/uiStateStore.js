/**
 * @name uiStateStore
 * @author Zac Steudel & Andrew Case
 *
 * @overview Contains the state for the workout logging & medicine logging.
 *
 */

import { observable, decorate } from 'mobx';

export default class UIStateStore {
	bottomNavigatorVisible = true;
	loading = false;
	page = 'Home';

	workoutDialogOpen = false;
	medicineDialogOpen = false;
	workoutsToday = null;
	workoutsThisWeek = [];
	medicineThisWeek = [];
	allWorkouts = [];
	allMedication = [];
	allFood = [];
	foodToday = null;
	medicineToday = null;
	lengthOfWorkingOutToday = 0;
	lengthOfMedicinesToday = 0;
	previouslyEnteredMeds = null;

	// food logging ui stuff
	foodDialogueOpen = false;
	foodLoggingPictureUri = null;

	//logging page accordions
	workoutLoggingAccordionOpen = false;
}

decorate(UIStateStore, {
	bottomNavigatorVisible: observable,
	loading: observable,
	page: observable,
	workoutDialogOpen: observable,
	medicineDialogOpen: observable,
	foodDialogueOpen: observable,
	foodLoggingPictureUri: observable,
	workoutsToday: observable,
	medicineToday: observable,
	lengthOfWorkingOutToday: observable,
	lengthOfMedicinesToday: observable,
	workoutLoggingAccordionOpen: observable,
	workoutsThisWeek: observable,
	medicineThisWeek: observable,
	allWorkouts: observable,
	allMedication: observable,
	allFood: observable,
	previouslyEnteredMeds: observable,
	foodToday: observable,
});
