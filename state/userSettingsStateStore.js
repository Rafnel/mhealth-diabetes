/**
 * @name userSettingsStateStore
 * @author Andrew Case & Zac Steudel
 *
 * @overview Contains the state for different settings that the user can change
 * on the settings page.
 *
 */

import { observable, decorate } from 'mobx';

export default class UserSettingsStateStore {
	language = "en";
	loggingAlerts = "true";
	loggingAlertsCurrentSession = "true";
}

decorate(UserSettingsStateStore, {
	language: observable,
	loggingAlerts: observable,
	loggingAlertsCurrentSession: observable
});
