/**
 * @name notificationStateStore
 * @author Zac Steudel
 *
 * @overview Contains the state for popup notifications.
 *
 */

import { observable, decorate, action } from 'mobx';

//state store for all success / error notifications to user.
export default class NotificationStateStore{
    successNotification = "";
    errorNotification = "";

    setSuccessNotification(notification){
        this.successNotification = notification;
    }

    setErrorNotification(notification){
        this.errorNotification = notification;
    }
}

//decorate this state store to be observable...
decorate(NotificationStateStore, {
    successNotification: observable,
    errorNotification: observable,
    setSuccessNotification: action, //actions are any functions that CHANGE our OBSERVABLE state variables.
    setErrorNotification: action
});

/* a note on actions... You can actually directly change observable state variables as well. You don't
 * actually need a "setter" function to change a state variable. I could say notifications.successNotification = "success!"
 * and this would be completely valid. Actions are really only useful for complex changes.
 */