/**
 * @name coinStateStore
 * @author Zac Steudel
 *
 * @overview Contains the state for the coin amount.
 *
 */

import { observable, decorate } from 'mobx';

export default class CoinStateStore{
    coins = 0;
}

decorate(CoinStateStore, {
    coins: observable
});