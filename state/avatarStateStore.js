/**
 * @name avatarStateStore
 * @author Zac Steudel
 *
 * @overview Contains the state of the avatar.
 *
 */

import { observable, decorate } from 'mobx';

export default class AvatarStateStore{
    hunger = "-";
    thirst = "-";
    exercise = "-";

    avatarUpdateFunction = null;
}

decorate(AvatarStateStore, {
    hunger: observable,
    thirst: observable,
    exercise: observable,
    avatarUpdateFunction: observable
});