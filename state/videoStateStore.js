/**
 * @name videoStateStore
 * @author Andrew Case
 *
 * @overview Contains the state for the current video and progress within the video.
 *
 */

import { observable, decorate } from 'mobx';

export default class VideoStateStore {
	currentVideo = 1;
	currentVideoProgress = 0;
}

decorate(VideoStateStore, {
	currentVideo: observable,
	currentVideoProgress: observable
});
