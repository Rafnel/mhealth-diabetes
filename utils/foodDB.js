/**
 * @name FoodDatabase
 * @author Mario Arturo Lopez Martinez
 * @overview SQLite database used to store information related to food logging
 */

import * as SQLite from 'expo-sqlite';

export default class FoodDatabase {
	db;

	constructor() {
		this.db = SQLite.openDatabase('foodDB.db');
	}

	//returns a db object, makes sure that the db is open/created correctly.
	initDB() {
		this.db.transaction(
			(tx) => {
				tx.executeSql(
					'CREATE TABLE IF NOT EXISTS foodDB \
				              (timestamp TEXT PRIMARY KEY, time TEXT, date TEXT, description TEXT, uri TEXT, isHealthy INTEGER)'
				);
			},
			(error) => {
				//there was an error, log it.
				console.log('There was an error creating FoodDB table.');
				console.log(error);
			},
			() => {
				//success callback
				console.log('Food Database/Table created/opened successfully.');
			}
		);
	}

	insertNewFood(description, uri, isHealthy) {
		let timestamp = new Date();
		timestamp = timestamp.toString();

		const date = getDateFormatted();
		const time = getTimeFormatted();

		//insert into the db.
		this.db.transaction(
			(tx) => {
				tx.executeSql(
					'insert into foodDB (timestamp, time, date, description, uri, isHealthy) values (?, ?, ?, ?, ?, ?)',
					[timestamp, time, date, description, uri, isHealthy]
				);
			},
			(error) => {
				console.log('Error entering the data into FoodDB. ' + error);
			},
			() => {
				//success.
				this.db.transaction((tx) => {
					tx.executeSql('select * from foodDB', [], (_, { rows }) => console.log(JSON.stringify(rows)));
				});
			}
		);
	}

	removeAllFood(uiState) {
		this.db.transaction((tx) => {
			tx.executeSql('delete from foodDB', [], (_, { rows }) => {
				uiState.foodToday = rows;
			});
		});
	}

	executeSQL = (sql, params = []) =>
		new Promise((resolve, reject) => {
			this.db.transaction((tx) => {
				tx.executeSql(
					sql,
					params,
					(db, results) => {
						resolve(results);
					},
					(err) => {
						reject(err);
					}
				);
			});
		});
}

export function getDateFormatted() {
	//get current date in mm/dd/yyyy format
	let today = new Date();
	let dd = today.getDate();
	let mm = today.getMonth() + 1;
	let yyyy = today.getFullYear();

	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}
	today = mm + '/' + dd + '/' + yyyy;

	return today;
}

export function getTimeFormatted() {
	//get current date in mm/dd/yyyy format
	let time = new Date();
	let HH = time.getHours();
	let MM = time.getMinutes();

	if (MM < 10) {
		MM = '0' + MM;
	}

	time = HH + ':' + MM;

	return time;
}
