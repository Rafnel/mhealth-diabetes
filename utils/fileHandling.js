import * as FileSystem from 'expo-file-system'
// create a path you want to write to
export function createFile(){
    var path = FileSystem.documentDirectory + "tester1234.txt";

    // write the file
    FileSystem.writeAsStringAsync(path, 'Lorem ipsum dolor sit amet')
      .then((success) => {
        console.log('FILE WRITTEN!');
        console.log(path);
      })
      .catch((err) => {
        console.log(err.message);
      });
}
