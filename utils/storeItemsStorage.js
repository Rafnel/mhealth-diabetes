import { AsyncStorage } from 'react-native';

export async function getOwnedStoreItems() {
    let ownedItems = await AsyncStorage.getItem("owned_Items");
    if(ownedItems) {
        ownedItems = JSON.parse(ownedItems);
    }
    return ownedItems;
}

export async function updateOwnedItems(storeItemsStateStore) {
    let currentlyOwned = await getOwnedStoreItems();
    if(currentlyOwned === null || currentlyOwned === "null") {
        currentlyOwned = {
            numOfOwned: 0,
            items: []
        };
    }

    await AsyncStorage.setItem("owned_Items", JSON.stringify(currentlyOwned));
    storeItemsStateStore.owned = currentlyOwned.items;
}

export async function addItemToOwned(newItem){
    newItem = newItem.toLowerCase();
    let currentlyOwned = await getOwnedStoreItems();
    currentlyOwned.numOfOwned += 1;
    currentlyOwned.items.push(newItem);
    await AsyncStorage.setItem("owned_Items", JSON.stringify(currentlyOwned));
}