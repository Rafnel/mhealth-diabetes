import { AsyncStorage } from 'react-native';


export async function getAvatarObject(){
    let avatarState = await AsyncStorage.getItem("avatar_state");
    //console.log("Just got avatar state: " + JSON.stringify(avatarState));
    if(avatarState){
        avatarState = JSON.parse(avatarState);
    }

    return avatarState;
}

//persistent state update function, gets called every 10 seconds
export async function updateAvatarState(avatarStateStore){
    let curState = await getAvatarObject();
    if(curState === null || curState === "null"){
        //set an initial state for the avatar.
        curState = {
            hunger: 0,
            thirst: 0,
            exercise: 1000,
            last_updated: new Date()
        };
    }
    else{
        //we do have an avatar state previously. update the avatar state and put.
        let now = new Date();
        let last_updated = new Date(curState.last_updated);
        let seconds = (now.getTime() - last_updated.getTime()) / 1000;

        //console.log("Now: " + now.toString() + ", previous update time: " + last_updated.toString() + ", seconds between: " + seconds);
        const updateValue = seconds / 100;

        curState.hunger += updateValue
        curState.thirst += updateValue;
        curState.exercise -= updateValue;
        curState.last_updated = now;
    }

    //put this in the state in AsyncStorage
    await AsyncStorage.setItem("avatar_state", JSON.stringify(curState));

    avatarStateStore.hunger = curState.hunger.toFixed(2);
    avatarStateStore.thirst = curState.thirst.toFixed(2);
    avatarStateStore.exercise = curState.exercise.toFixed(2);
    
}

export async function updateExercise(minutesExercise){
    //when user enters exercise, this function should be called to increase the avatar's exercise levels.
    let curState = await getAvatarObject();
    curState.exercise += minutesExercise * 10;
    await AsyncStorage.setItem("avatar_state", JSON.stringify(curState));
}