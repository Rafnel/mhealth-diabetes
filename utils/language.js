//this file will handle returning either english or hindi text based on user language settings.

//interactions page
export function currentVideoMessage(language, currentVideo){
    let currentVideoName = "";

    if(language === "en"){
        switch(currentVideo){
            case 1: currentVideoName = "Introduction to Diabetes";
                                        break;
            case 2: currentVideoName = "Symptoms of Diabetes";
                                        break;
            case 3: currentVideoName = "Preventing and Managing Diabetes";
                                        break;
            case 4: currentVideoName = "Good Eating Habits";
                                        break;
        }
    }
    else{
        switch(currentVideo){
            case 1: currentVideoName = "मधुमेह का परिचय";
                                        break;
            case 2: currentVideoName = "मधुमेह के लक्षण";
                                        break;
            case 3: currentVideoName = "मधुमेह की रोकथाम और प्रबंधन";
                                        break;
            case 4: currentVideoName = "गुड खाने की आदत";
                                        break;
        }
    }

    if(language === "en"){
        return (currentVideo === 5 ? "Current Video: Done! Re-watch old videos and play educational games." : "Current Video: " + currentVideoName)
    }
    else{
        return (currentVideo === 5 ? "वर्तमान वीडियो: हो गया! पुराने वीडियो फिर से देखें और शैक्षिक खेल खेलें।" : "वर्तमान वीडियो: " + currentVideoName)
    }
}

export function getVideoProgress(language, videoProgress){
    if(language === "en"){
        return ("Current Video Progress: " + (videoProgress / 1000).toFixed(0) + " seconds");
    }
    else{
        return ("वर्तमान वीडियो प्रगति: " + (videoProgress / 1000).toFixed(0) + " सेकंड");
    }
}