import * as SQLite from 'expo-sqlite';

export default class MedicineDatabase {
	db;
	constructor() {
		this.db = SQLite.openDatabase("medicineDB.db");
	}
	//returns a db object, makes sure that the db is open/created correctly.
	initDB() {
		this.db.transaction(tx => {
			tx.executeSql('CREATE TABLE IF NOT EXISTS medicineDB \
                              (timestamp TEXT PRIMARY KEY, date TEXT, dosage TEXT, name TEXT)');
		},
			(error) => {
				//there was an error, log it.
				console.log("There was an error creating table.");
				console.log(error);
			},
			() => {
				//success callback
				console.log("Database/Table created/opened successfully.")
			});

	};

	insertNewMedicine(dosage, name, uiState) {
		let timestamp = new Date();
		timestamp = timestamp.toString();
		const date = getDateFormatted();

		console.log("Medicine dosage: " + dosage + ", medicine name: " + name + ", timestamp: " + timestamp + ", date: " + date);

		//insert into the db.
		this.db.transaction(tx => {
			tx.executeSql("insert into medicineDB (timestamp, date, dosage, name) values (?, ?, ?, ?)", [timestamp, date, dosage, name]);
		},
			(error) => {
				console.log("Error entering the data." + error);
			},
			() => {
				//success.
				this.db.transaction(
					tx => {
						tx.executeSql("select * from medicineDB", [], (_, { rows }) =>
							console.log(JSON.stringify(rows))
						);
					});
				this.getMedicineFromToday(uiState);
			});
	}

	getMedicineFromToday(uiState) {
		const date = getDateFormatted();

		this.db.transaction(tx => {
			tx.executeSql("select * from medicineDB where date = ?", [date], (_, { rows }) => {
				console.log(JSON.stringify(rows));
				uiState.medicineToday = rows;

				// Update medication taken today
				uiState.lengthOfMedicinesToday = 0;
				for (let i = 0; i < rows._array.length; i++) {
					uiState.lengthOfMedicinesToday++;
					// uiState.lengthOfMedicinesToday += rows._array[i].length;
				}
			});
		});
	}

	executeSQL = (sql, params = []) => new Promise((resolve, reject) =>{
		this.db.transaction((tx) => {
			tx.executeSql(sql, params, (db, results) => {
				resolve(results);
			}, (err) => {
				reject(err);
			});
		});
	});

	removeAllMedicine(uiState) {
		const date = getDateFormatted();

		this.db.transaction( tx => {
			tx.executeSql("delete from medicineDB", [], (_, { rows }) => {
				uiState.medicineToday = rows;
				uiState.lengthOfMedicinesToday = 0;
			});
		});
	}

	previouslyEnteredMedication(uiState) {
		this.db.transaction(tx => {
			tx.executeSql("Select distinct name from medicineDB", [], (_, { rows }) => {
				console.log(JSON.stringify(rows));
				uiState.previouslyEnteredMeds = rows;
			});
		});
	}

	async getMedicineThisWeek(uiState) {
		//function will put all workouts in uiState that occurred from this Monday (day 1) to this Sunday (day 0)
		//get the day of week.
		const currentDate = new Date();
		const currentDayOfWeek = currentDate.getDay();
		console.log("Current day of week: " + currentDayOfWeek);

		
		//We need to get the date of every previous day this week through Monday
		//and get all workouts from that day.
		let todayDate = getDateFormatted();
		let results = await this.executeSQL("select * from medicineDB where date = ?", [todayDate]);
		//clear the workouts array
		uiState.medicineThisWeek = [];
		//push the workouts from today to the array
		Array.prototype.push.apply(uiState.medicineThisWeek, results.rows._array);
		console.log("Medicine for " + todayDate + ": " + JSON.stringify(results.rows._array));

		//if its monday, we are done 
		if(currentDayOfWeek === 1){
			return;
		}

		for(let i = currentDayOfWeek; i > 0; i--){
			let thisDay = new Date();
			thisDay.setDate(thisDay.getDate() - i);
			thisDay = formatDate(thisDay);

			results = await this.executeSQL("select * from medicineDB where date = ?", [thisDay]);
			console.log("Medicine for " + thisDay + ": " + JSON.stringify(results.rows._array));
			//push workouts from this day to array
			uiState.medicineThisWeek = uiState.medicineThisWeek.concat(results.rows._array);

			console.log("FINAL medicine this week: " + JSON.stringify(uiState.medicineThisWeek));
		}
	}
}

export function formatDate(date){
    //function formats the date object to be mm/dd/yyyy string
    let dd = date.getDate();

    let mm = date.getMonth() + 1; 
    let yyyy = date.getFullYear();
    if(dd < 10) 
    {
        dd = '0' + dd;
    } 

    if(mm < 10) 
    {
        mm = '0' + mm;
    } 
    date = mm + '/' + dd + '/' + yyyy;

    return date;
}

export function getDateFormatted() {
	//get current date in mm/dd/yyyy format
	let today = new Date();
	let dd = today.getDate();

	let mm = today.getMonth() + 1;
	let yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}
	today = mm + '/' + dd + '/' + yyyy;

	return today;
}