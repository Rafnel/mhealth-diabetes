import { AsyncStorage } from 'react-native';

export async function getHealthCoins(){
    let coins = await AsyncStorage.getItem("coins");
    
    //check if coins have been initialized before for this user
    if(coins == undefined){
        //give user initial value of 5 health coins
        await updateHealthCoins(5);
        coins = "5";
    }

    let coinsInt = parseInt(coins);
    return coinsInt;
}

export async function updateHealthCoins(newCoinValue){
    let coinsString = newCoinValue.toString(10);
    console.log("writing new coin value: " + coinsString)
    await AsyncStorage.setItem("coins", coinsString);
}