import { AsyncStorage } from 'react-native';

export async function getVideosObject() {
	let videosState = await AsyncStorage.getItem('videos_state');
	if (videosState) {
		videosState = JSON.parse(videosState);
	}
	return videosState;
}

export async function updateVideosState(videosStateStore) {
	let currentState = await getVideosObject();
	if (currentState === null || currentState === 'null') {
		//set an initial state for the videos.
		console.log("Setting video states to beginning...")
		currentState = {
			currentVideo: 1,
			currentVideoProgress: 0
		};
	} else {
		currentState.currentVideo = videosStateStore.currentVideo;
		currentState.currentVideoProgress =
			videosStateStore.currentVideoProgress;
	}
	// Put update in the AsyncStorage
	await AsyncStorage.setItem('videos_state', JSON.stringify(currentState));
	currentState = await getVideosObject();
	console.log("Video state now after update: " + JSON.stringify(currentState));
}