import * as SQLite from 'expo-sqlite';

export default class WorkoutsDatabase {
	db;
	constructor() {
		this.db = SQLite.openDatabase('workoutsDB.db');
	}
	//returns a db object, makes sure that the db is open/created correctly.
	initDB() {
		this.db.transaction(
			(tx) => {
				tx.executeSql(
					'CREATE TABLE IF NOT EXISTS workoutsDB \
                              (timestamp TEXT PRIMARY KEY, date TEXT, type TEXT, length INTEGER)'
				);
			},
			(error) => {
				//there was an error, log it.
				console.log('There was an error creating table.');
				console.log(error);
			},
			() => {
				//success callback
				console.log('Database/Table created/opened successfully.');
			}
		);
	}

	insertNewWorkout(type, length, uiState) {
		length = Math.trunc(length);
		let timestamp = new Date();
		timestamp = timestamp.toString();
		const date = getDateFormatted();

		console.log(
			'Workout type: ' + type + ', workout length: ' + length + ', timestamp: ' + timestamp + ', date: ' + date
		);

		//insert into the db.
		this.db.transaction(
			(tx) => {
				tx.executeSql('insert into workoutsDB (timestamp, date, type, length) values (?, ?, ?, ?)', [
					timestamp,
					date,
					type,
					length,
				]);
			},
			(error) => {
				console.log('Error entering the data.' + error);
			},
			() => {
				//success.
				/*this.db.transaction(
                tx => {
                  tx.executeSql("select * from workoutsDB", [], (_, { rows }) =>
                    console.log(JSON.stringify(rows))
                  );
            });*/
				this.getWorkoutsFromToday(uiState);
			}
		);
	}

	getWorkoutsFromToday(uiState) {
		const date = getDateFormatted();

		this.db.transaction((tx) => {
			tx.executeSql('select * from workoutsDB where date = ?', [date], (_, { rows }) => {
				console.log("Workouts today: " + JSON.stringify(rows));
				uiState.workoutsToday = rows;

				//update minutes spent working out today in ui state
				uiState.lengthOfWorkingOutToday = 0;
				for (let i = 0; i < rows._array.length; i++) {
					uiState.lengthOfWorkingOutToday += rows._array[i].length;
				}
			});
		});
	}

	removeAllWorkouts(uiState) {
		const date = getDateFormatted();

		this.db.transaction((tx) => {
			tx.executeSql('delete from workoutsDB', [], (_, { rows }) => {
				uiState.workoutsToday = rows;

				// Update medication taken today
				uiState.lengthOfWorkingOutToday = 0;
			});
		});
	}

	executeSQL = (sql, params = []) =>
		new Promise((resolve, reject) => {
			this.db.transaction((tx) => {
				tx.executeSql(
					sql,
					params,
					(db, results) => {
						resolve(results);
					},
					(err) => {
						reject(err);
					}
				);
			});
		});

	async getWorkoutsThisWeek(uiState) {
		//function will put all workouts in uiState that occurred from this Monday (day 1) to this Sunday (day 0)
		//get the day of week.
		const currentDate = new Date();
		const currentDayOfWeek = currentDate.getDay();
		console.log('Current day of week: ' + currentDayOfWeek);

		//We need to get the date of every previous day this week through Monday
		//and get all workouts from that day.
		let todayDate = getDateFormatted();
		let results = await this.executeSQL('select * from workoutsDB where date = ?', [todayDate]);
		//clear the workouts array
		uiState.workoutsThisWeek = [];
		//push the workouts from today to the array
		Array.prototype.push.apply(uiState.workoutsThisWeek, results.rows._array);
		console.log('Workouts for ' + todayDate + ': ' + JSON.stringify(results.rows._array));

		//if it is monday, we are done
		if (currentDayOfWeek === 1) {
			return;
		}

		for (let i = currentDayOfWeek; i > 0; i--) {
			let thisDay = new Date();
			thisDay.setDate(thisDay.getDate() - i);
			thisDay = formatDate(thisDay);

			results = await this.executeSQL('select * from workoutsDB where date = ?', [thisDay]);
			console.log('Workouts for ' + thisDay + ': ' + JSON.stringify(results.rows._array));
			//push workouts from this day to array
			uiState.workoutsThisWeek = uiState.workoutsThisWeek.concat(results.rows._array);
		}

		console.log('FINAL workouts this week: ' + JSON.stringify(uiState.workoutsThisWeek));
	}
}

export function formatDate(date) {
	//function formats the date object to be mm/dd/yyyy string
	let dd = date.getDate();

	let mm = date.getMonth() + 1;
	let yyyy = date.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}
	date = mm + '/' + dd + '/' + yyyy;

	return date;
}

export function getDateFormatted() {
	//get current date in mm/dd/yyyy format
	let today = new Date();
	let dd = today.getDate();

	let mm = today.getMonth() + 1;
	let yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}
	today = mm + '/' + dd + '/' + yyyy;

	return today;
}
