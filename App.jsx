// Libraries
import React from 'react';
import { Provider } from 'mobx-react';
import { Provider as PaperProvider } from 'react-native-paper';
// Custom Components
import CustomTabBar from './components/CustomTabBar';
import ErrorMessage from './components/ErrorMessage';
import SuccessMessage from './components/SuccessMessage';
import StackNavigator from './components/StackNavigator';
// MobX State
import UIStateStore from './state/uiStateStore';
import CoinStateStore from './state/coinStateStore';
import VideosStateStore from './state/videoStateStore';
import AvatarStateStore from './state/avatarStateStore';
import StoreItemsStateStore from './state/storeItemsStateStore';
import NotificationStateStore from './state/notificationStateStore';
import UserSettingsStateStore from './state/userSettingsStateStore';

export default class App extends React.Component {
	render() {
		return (
			<Provider
				notifications={new NotificationStateStore()}
				ui={new UIStateStore()}
				userSettings={new UserSettingsStateStore()}
				avatar={new AvatarStateStore()}
				videos={new VideosStateStore()}
				coinState={new CoinStateStore()}
				storeItems={new StoreItemsStateStore()}
			>
				<PaperProvider>
					<SuccessMessage />
					<StackNavigator />
					<ErrorMessage />
					<CustomTabBar />
				</PaperProvider>
			</Provider>
		);
	}
}
