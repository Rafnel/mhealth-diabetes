import { StyleSheet } from 'react-native';

export const mainStyle = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	}
});

export const homeStyle = StyleSheet.create({
	container: {
		alignItems: 'center',
		//justifyContent: 'center',
		flex:1
	}
});

export const videoStyles = StyleSheet.create({
	controlBar: {
		position: 'relative',
		bottom: 0,
		left: 0,
		right: 0,
		height: 45,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'rgba(0, 0, 0, 0.5)'
	}
});

export const storeImageStyle = StyleSheet.create({
	image: {
		width: 100,
		height: 100
	},
	alreadyOwned: {
		width: 50,
		height: 50
	}
});
