import React from 'react';
import { Text, View, ScrollView, AsyncStorage, Alert } from 'react-native';
import { homeStyle } from './Styling';
import { inject, observer } from 'mobx-react';
import HomePageQuestions from './HomePageQuestions';
import Avatar from '../components/avatar/Avatar';
import { getHealthCoins } from '../utils/coinStorage';
import Store from '../components/Store';
import * as RootNavigation from '../state/RootNavigation';
import MedicineDatabase from '../utils/medicineDB';
import WorkoutsDatabase from '../utils/workoutDB';
import FoodDatabase from '../utils/foodDB';

const HomePage = class HomePage extends React.Component {
	constructor() {
		super();
		this.state = {
			firstLaunch: null,
			loaded: 'false',
			diabetesCoins: null,
		};
	}

	loggingAlert() {
		if (
			this.props.userSettings.loggingAlerts === 'true' &&
			this.props.userSettings.loggingAlertsCurrentSession === 'true' &&
			this.props.ui.lengthOfMedicinesToday === 0 &&
			this.props.ui.lengthOfWorkingOutToday === 0
		) {
			if (this.props.userSettings.language === 'en') {
				Alert.alert(
					'Logging Alert',
					'Have you exercised or taken your daily medication(s) today?\nIf so, log it and get some health coins!',
					[
						{
							text: 'Ask me later',
							onPress: () => (this.props.userSettings.loggingAlertsCurrentSession = 'false'),
						},
						{ text: 'Yes', onPress: () => RootNavigation.navigate('Logging') },
						{
							text: 'No',
							onPress: () => (this.props.userSettings.loggingAlertsCurrentSession = 'false'),
							style: 'cancel',
						},
					],
					{ cancelable: false }
				);
			} else if (this.props.userSettings.language === 'hi') {
				Alert.alert(
					'लॉगिंग अलर्ट',
					'क्या आपने आज अपनी दैनिक दवा (व्यायाम) ली है?\nयदि हां, तो इसे लॉग इन करें और कुछ स्वास्थ्य सिक्के प्राप्त करें!',
					[
						{
							text: 'मुझसे बाद में पूछना',
							onPress: () => (this.props.userSettings.loggingAlertsCurrentSession = 'false'),
						},
						{ text: 'हाँ', onPress: () => RootNavigation.navigate('लॉगिंग') },
						{
							text: 'नहीं',
							onPress: () => (this.props.userSettings.loggingAlertsCurrentSession = 'false'),
							style: 'cancel',
						},
					],
					{ cancelable: false }
				);
			}
		}
	}

	_setValue = async () => {
		await AsyncStorage.setItem('demographicsSet', 'false');
		await AsyncStorage.setItem('diabetesCoins', '0.0');
	};

	_loadInitialState = async () => {
		try {
			let value = await AsyncStorage.getItem('demographicsSet');
			if (value === 'true') {
				this.setState({ loaded: 'true', firstLaunch: 'false' });
			} else {
				this.setState({ loaded: 'true', firstLaunch: 'true' });
			}
		} catch (error) {
			this.setState({ loaded: 'false' });
			this._setValue();
		}
	};

	async componentDidMount() {
		this._loadInitialState().done();
		//get coins from AsyncStorage
		this.props.coinState.coins = await getHealthCoins();
		this.props.userSettings.loggingAlerts = await AsyncStorage.getItem('loggingAlerts');
		if (this.props.userSettings.loggingAlerts === null) {
			this.props.userSettings.loggingAlerts = 'true';
			AsyncStorage.setItem('loggingAlerts', this.props.userSettings.loggingAlerts);
		}
		this.props.userSettings.language = await AsyncStorage.getItem('language');
		let medicineDBService = new MedicineDatabase();
		medicineDBService.initDB();
		medicineDBService.getMedicineFromToday(this.props.ui);
		let workoutsDBService = new WorkoutsDatabase();
		workoutsDBService.initDB();
		workoutsDBService.getWorkoutsFromToday(this.props.ui);
		let foodDBService = new FoodDatabase();
		foodDBService.initDB();
		// @TODO Get food from last few days
		this.loggingAlert();
		console.log('Just got health coins, user has ' + this.props.coinState.coins);
	}

	returnUserWelcomeMessage = () => {
		if (this.props.userSettings.language === 'en') {
			return 'Welcome to the Baylor University Mobile Health Diabetes App! Below, you have a pet that you can keep healthy by logging your own workouts, medicine intake, and meals.';
		} else {
			return 'Baylor विश्वविद्यालय मोबाइल स्वास्थ्य मधुमेह ऐप में आपका स्वागत है! नीचे, आपके पास एक पालतू जानवर है जिसे आप अपने वर्कआउट, दवाई, और भोजन में लॉग इन करके स्वस्थ रख सकते हैं।';
		}
	};

	render() {
		if (this.state.loaded === 'false') {
			return (
				<View>
					<Text>Loading...</Text>
				</View>
			);
		} else {
			if (this.state.firstLaunch === 'true') {
				return <HomePageQuestions props={this.props} loadAgain={this._loadInitialState}></HomePageQuestions>;
			} else if (this.state.firstLaunch === 'false') {
				let lang = this.props.userSettings.language;
				return (
					<View style={homeStyle.container}>
						<ScrollView contentContainerStyle={{ paddingBottom: 100 }}>
							<Text style={{ padding: 10 }}>{this.returnUserWelcomeMessage()}</Text>
							<Avatar />
							<Text>&nbsp;</Text>
							<Store />
						</ScrollView>
					</View>
				);
			}
		}
	}
};

export default inject('ui', 'coinState', 'userSettings')(observer(HomePage));
