/**
 * @name LoggingPage
 * @author Zac Steudel & Andrew Case
 *
 * @overview A scrollview containing the various logging surfaces.
 *
 * @example
 * Used in CustomTabBar for navigation.
 * RootNavigation.navigate("LoggingPage");
 */

import React from 'react';
import { inject, observer } from 'mobx-react';
import { View, Text, ScrollView, AsyncStorage } from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import WorkoutLoggingSurface from '../components/WorkoutLoggingSurface'
import MedicineLoggingSurface from '../components/MedicineLoggingSurface'
import FoodLoggingSurface from '../components/FoodLoggingSurface'
import { Title } from 'react-native-paper';

//{lang === "en" ? "View and Log Workouts" : "वर्कआउट देखें और लॉग करें"}

const LoggingPage = class LoggingPage extends React.Component {
	state = {
		activeSections: [],
	};
	 
	_renderSectionTitle = section => {
		return null;
	};
	 
	_renderHeader = section => {
		return (
			<View style = {{alignItems: 'center'}}>
				<Title>{section.title}</Title>
			</View>
		);
	};
	 
	_renderContent = section => {
		return (
			<View>
				{section.content}
			</View>
		);
	};
	 
	_updateSections = activeSections => {
		this.setState({ activeSections });
	};
	 
	render() {
		let lang = this.props.userSettings.language;
		return ( 
			<ScrollView>
				<Text style = {{padding: 10, backgroundColor: 'white'}}>{lang === "en" ? "Tap any of the below buttons to log food, workouts, and medicine and get rewards!" : "भोजन, वर्कआउट, और चिकित्सा लॉग इन करने और पुरस्कार पाने के लिए नीचे दिए गए किसी भी बटन को टैप करें!"}</Text>

				<Accordion
					underlayColor={"#727272"}
					sections={[
						{
						  title: lang === "en" ? "View and Log Workouts" : "वर्कआउट देखें और लॉग करें",
						  content: <WorkoutLoggingSurface/>,
						},
						{
						  title: lang === "en" ? 'View and Log Meals' : "भोजन देखें और लॉग करें",
						  content: <FoodLoggingSurface/>,
						},
						{
							title: lang === "en" ? "View and Log Prescription Medicines" : "प्रिस्क्रिप्शन दवाएं देखें और लॉग करें",
							content: <MedicineLoggingSurface/>,
						},
					]}
					activeSections={this.state.activeSections}
					renderSectionTitle={this._renderSectionTitle}
					renderHeader={this._renderHeader}
					renderContent={this._renderContent}
					onChange={this._updateSections}
					expandMultiple = {true}
				/>

				{/* purpose of this 50 pixel margin is that the app overlaps with the tab bar w/o it. */}
				<Text style = {{height: 50}}/>
			</ScrollView>
			
		);
	}

	async componentDidMount() {
		this.props.userSettings.language = await AsyncStorage.getItem("language");
	}
}

export default inject('ui', 'userSettings')(observer(LoggingPage));
