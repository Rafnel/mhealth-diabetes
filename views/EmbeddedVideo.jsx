import React from 'react';
import { Dimensions } from 'react-native';
import { Video } from 'expo-av';

export const EmbeddedVideo = class EmbeddedVideo extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			currentPosition: 0,
			videoDuration: -1
		}
	}

	// If the embedded video is left, and its not because it finished, 
	// the callback function will be invoked
	componentWillUnmount() {
		if (this.state.currentPosition !== this.state.videoDuration) {
			this.props.finishedPlaying(this.state.currentPosition);
		}
	}

	// This function will change whenever the status of the video is changed
	_onPlaybackStatusUpdate = playbackStatus => {
		// If the player has just finished playing, perform callback
		if (playbackStatus.didJustFinish) {
			this.props.finishedPlaying("Finished");
		}
		// If the player is paused, save the current position of the video
		else if (!playbackStatus.isPlaying) {
			this.setState({ videoDuration: playbackStatus.durationMillis });
			this.setState({ currentPosition: playbackStatus.positionMillis });
		}
	}

	render() {
		let { height, width } = Dimensions.get('window');
		return (
			<Video
				source={this.props.path}
				rate={1.0}
				volume={1.0}
				shouldPlay={false}
				resizeMode="cover"
				isMuted={false}
				videoPlaying={this.props.videoPlaying}
				// isLooping
				style={{ width: width, height: 250 }}
				useNativeControls={true}
				positionMillis={this.props.currentPosition}
				onPlaybackStatusUpdate={(playbackStatus) => this._onPlaybackStatusUpdate(playbackStatus)}
			/>
		);
	}
};