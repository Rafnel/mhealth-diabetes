import React from 'react';
import { inject, observer } from 'mobx-react';
import { Text, View, AsyncStorage, Linking, ScrollView, StyleSheet } from 'react-native';
import LanguageSwitch from '../components/LanguageSwitch';
import LoggingAlertSwitch from '../components/LoggingAlertSwitch';
import { Divider, Subheading, Button, TextInput, Portal, Dialog } from 'react-native-paper';
import email from 'react-native-email';
import WorkoutsDatabase from '../utils/workoutDB';
import { dateddmmyyyy } from './WorkoutHistory';
import MedicineDatabase from '../utils/medicineDB';
import FoodDatabase from '../utils/foodDB';

const SettingsPage = class SettingsPage extends React.Component {
	state = {
		menuVisible: false,
		userEmail: '',
	};
	handleChange = (value) => {
		this.props.userSettings.language === 'en'
			? (this.props.userSettings.language = 'hi')
			: (this.props.userSettings.language = 'en');

		//update the language setting in AsyncStorage
		AsyncStorage.setItem('language', this.props.userSettings.language);
	};

	addCoins() {
		this.props.coinState.coins += 10;
	}

	setMenuVisible = () => {
		this.setState({ menuVisible: true });
	};

	exportUserData = async () => {
		const to = [this.state.userEmail];
		//get all workouts
		let workoutDBService = new WorkoutsDatabase();
		workoutDBService.initDB();
		let results = await workoutDBService.executeSQL('select * from workoutsDB', []);
		let allWorkouts = results.rows._array;
		//format the email body
		if (this.props.userSettings.language === 'en') {
			let emailBody = 'Workouts:\n';
			for (let i = 0; i < allWorkouts.length; i++) {
				emailBody +=
					allWorkouts[i].date + ': ' + allWorkouts[i].type + ', ' + allWorkouts[i].length + ' minutes\n';
			}
			email(to, {
				body: emailBody,
			});
		} else {
			//need some translating for hindi
			let emailBody = 'व्यायाम:\n';
			for (let i = 0; i < allWorkouts.length; i++) {
				let date = dateddmmyyyy(allWorkouts[i].date);
				//change type of workout for hindi
				let hindiWorkout;
				switch (allWorkouts[i].type) {
					case 'Yoga':
						hindiWorkout = 'योग';
						break;
					case 'Walking':
						hindiWorkout = 'चलना';
						break;
					case 'Running':
						hindiWorkout = 'चल रहा है';
						break;
					case 'Swimming':
						hindiWorkout = 'तैराकी';
						break;
					case 'Sport':
						hindiWorkout = 'खेल';
						break;
					case 'Other':
						hindiWorkout = 'अन्य';
						break;
				}
				emailBody += date + ': ' + hindiWorkout + ', ' + allWorkouts[i].length + ' मिनट\n';
			}
			email(to, {
				body: emailBody,
			});
		}

		this.setState({ menuVisible: false });
	};

	deleteAllMedication() {
		let medicineDBService = new MedicineDatabase();
		medicineDBService.initDB();
		medicineDBService.removeAllMedicine(this.props.ui);
	}

	deleteAllWorkouts() {
		let workoutDBService = new WorkoutsDatabase();
		workoutDBService.initDB();
		workoutDBService.removeAllWorkouts(this.props.ui);
	}

	deleteAllFood() {
		let foodDBService = new FoodDatabase();
		foodDBService.initDB();
		foodDBService.removeAllFood(this.props.ui);
	}

	render() {
		let lang = this.props.userSettings.language;
		return (
			<ScrollView contentContainerStyle={styles.scrollview}>
				<Text />
				<LanguageSwitch />
				<Text />
				<LoggingAlertSwitch />
				<Text />

				<Button icon="file-export" mode="contained" onPress={this.setMenuVisible}>
					{lang === 'en' ? 'Export Data' : 'निर्यात जानकारी'}
				</Button>
				<Text />
				<Divider style={{ width: '100%', backgroundColor: '#000000' }} />
				<Subheading>{lang === 'en' ? 'More Information' : 'नअधिक जानकारी'}</Subheading>
				<Button mode="text" disabled="true" uppercase={true} onPress={() => Linking.openURL('http://google.com')}>
					{lang === 'en' ? 'Privacy Policy' : 'गोपनीयता नीति'}
				</Button>
				<Button mode="text" disabled="true" uppercase={true} onPress={() => Linking.openURL('http://google.com')}>
					{lang === 'en' ? 'Terms of Use' : 'उपयोग की शर्तें'}
				</Button>
				<Button
					// icon={require('../assets/icon.png')}
					mode="text"
					uppercase={true}
					onPress={() =>
						Linking.openURL('https://www.seekpng.com/ipng/u2w7w7i1t4a9t4r5_all-type-2-diabetes-icon/')
					}
				>
					{lang === 'en' ? 'App Icon Use' : 'ऐप आइकन का उपयोग करें'}
				</Button>
				<Text />
				<Text>{lang === 'en' ? 'mHealth Diabetes' : 'mHealth मधुमेह'} v1.0.0</Text>
				<Text>{lang === 'en' ? 'Made by' : 'द्वारा निर्मित'} Baylor University</Text>

				<Divider style={{ width: '100%', backgroundColor: '#000000' }} />
				<Subheading>Dev Buttons</Subheading>
				<Text />
				<Button
					mode="outlined"
					uppercase={false}
					onPress={() => AsyncStorage.setItem('demographicsSet', 'false')}
				>
					Reset User Settings
				</Button>
				<Text>&nbsp;</Text>
				<Button mode="outlined" uppercase={false} onPress={() => AsyncStorage.setItem('avatar_state', 'null')}>
					Reset Pet State
				</Button>
				<Text>&nbsp;</Text>
				<Button mode="outlined" uppercase={false} onPress={() => AsyncStorage.setItem('owned_Items', 'null')}>
					Reset Store State
				</Button>
				<Text>&nbsp;</Text>
				<Button mode="outlined" uppercase={false} onPress={() => this.addCoins()}>
					Add Ten Coins
				</Button>
				<Text>&nbsp;</Text>
				<Button mode="outlined" uppercase={false} onPress={() => this.deleteAllMedication()}>
					Delete Medication Log
				</Button>
				<Text>&nbsp;</Text>
				<Button mode="outlined" uppercase={false} onPress={() => this.deleteAllFood()}>
					Delete Food Log
				</Button>
				<Text>&nbsp;</Text>
				<Button
					mode="outlined"
					uppercase={false}
					onPress={() => {
						AsyncStorage.clear(),
							this.deleteAllMedication(),
							this.deleteAllWorkouts(),
							this.deleteAllFood();
					}}
				>
					Delete All Local Storage {'&'} Databases
				</Button>
				<Text style={{ height: 55 }}>&nbsp;</Text>

				<Portal>
					<Dialog visible={this.state.menuVisible} onDismiss={() => this.setState({ menuVisible: false })}>
						<Dialog.Title>
							{lang === 'en' ? 'Export Logged Data' : 'निर्यात डेटा लॉग किया गया'}
						</Dialog.Title>
						<Dialog.Content>
							<Text>
								{lang === 'en'
									? 'Please enter the email address that you would like to have your logged data be sent to.'
									: 'कृपया वह ईमेल पता दर्ज करें जिसे आप चाहते हैं कि आपका लॉग किया गया डेटा भेजा जाए।'}
							</Text>
							<Text />
							<TextInput
								label={lang === 'en' ? 'Email' : 'ईमेल'}
								mode="outlined"
								value={this.state.userEmail}
								onChangeText={(text) => this.setState({ userEmail: text })}
							/>
						</Dialog.Content>
						<Dialog.Actions>
							<Button onPress={() => this.setState({ menuVisible: false })} mode="outlined">
								{lang === 'en' ? 'Cancel' : 'रद्द करना'}
							</Button>

							<Text>&nbsp;</Text>

							<Button mode="contained" onPress={this.exportUserData}>
								{lang === 'en' ? 'Export' : 'निर्यात'}
							</Button>
						</Dialog.Actions>
					</Dialog>
				</Portal>
			</ScrollView>
		);
	}

	async componentDidMount() {
		this.props.userSettings.language = await AsyncStorage.getItem('language');
	}
};

const styles = StyleSheet.create({
	scrollview: {
		justifyContent: 'center',
		alignItems: 'center',
	},
});

export default inject('ui', 'userSettings', 'coinState')(observer(SettingsPage));
