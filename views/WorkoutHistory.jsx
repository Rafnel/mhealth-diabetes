import { Text, ScrollView } from 'react-native';
import React from 'react';
import { inject, observer } from 'mobx-react';
import { Surface, Divider } from 'react-native-paper';
import WorkoutsDatabase from '../utils/workoutDB';

const WorkoutHistory = class WorkoutHistory extends React.Component {
	returnAllWorkouts = () => {
		console.log(JSON.stringify(this.props.ui.allWorkouts));
		let toReturn = [];

		for (let i = 0; i < this.props.ui.allWorkouts.length; i++) {
			let workout = this.props.ui.allWorkouts[i];
			if (this.props.userSettings.language === 'en') {
				toReturn.push(
					<Text key={i}>{workout.date + ': ' + workout.type + ', ' + workout.length + ' minutes'}</Text>
				);
			} else {
				//need to change the date to dd/mm/yyyy since that is common in hindi
				let date = dateddmmyyyy(workout.date);
				//change type of workout for hindi
				let hindiWorkout;
				switch (workout.type) {
					case 'Yoga':
						hindiWorkout = 'योग';
						break;
					case 'Walking':
						hindiWorkout = 'चलना';
						break;
					case 'Running':
						hindiWorkout = 'चल रहा है';
						break;
					case 'Swimming':
						hindiWorkout = 'तैराकी';
						break;
					case 'Sport':
						hindiWorkout = 'खेल';
						break;
					case 'Other':
						hindiWorkout = 'अन्य';
						break;
				}
				toReturn.push(<Text key={i}>{date + ': ' + hindiWorkout + ', ' + workout.length + ' मिनट'}</Text>);
			}
		}

		return toReturn;
	};
	render() {
		let lang = this.props.userSettings.language;
		return (
			<ScrollView>
				<Surface style={{ elevation: 3, padding: 10, marginTop: 10 }}>
					<Text>
						{lang === 'en'
							? 'All of your workouts from most recent to least recent are available here.'
							: 'आपके सभी वर्कआउट सबसे हाल ही में कम से कम हाल ही में यहां उपलब्ध हैं।'}
					</Text>
					<Divider style={{ width: 600 }} />
					{this.returnAllWorkouts()}
				</Surface>

				<Text style={{ height: 50 }} />
			</ScrollView>
		);
	}

	async componentDidMount() {
		let workoutDBService = new WorkoutsDatabase();
		workoutDBService.initDB();
		let results = await workoutDBService.executeSQL('select * from workoutsDB', []);
		this.props.ui.allWorkouts = results.rows._array;

		const workoutsCopy = [...this.props.ui.allWorkouts];
		workoutsCopy.sort((workoutA, workoutB) => (workoutA.date > workoutB.date ? -1 : 1));
		this.props.ui.allWorkouts = workoutsCopy;
	}
};

//function takes a mm/dd/yyyy date and returns dd/mm/yyyy date
export function dateddmmyyyy(date) {
	String.prototype.replaceAt = function (index, replacement) {
		return this.substr(0, index) + replacement + this.substr(index + replacement.length);
	};

	let month = date.substr(0, 2);
	let day = date.substr(3, 2);
	date = date.replaceAt(0, day.charAt(0));
	date = date.replaceAt(1, day.charAt(1));
	date = date.replaceAt(3, month.charAt(0));
	date = date.replaceAt(4, month.charAt(1));

	return date;
}

export default inject('ui', 'userSettings')(observer(WorkoutHistory));
