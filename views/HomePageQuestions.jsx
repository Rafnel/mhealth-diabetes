import React from 'react';
import {
	StyleSheet,
	Picker,
	AsyncStorage,
	Animated,
	Dimensions
} from 'react-native';
import { inject, observer } from 'mobx-react';
import { setUserSettings } from '../api/userAPI';
import Constants from 'expo-constants';
import { Divider, Subheading, Button } from 'react-native-paper';

export class HomePageQuestions extends React.Component {
	constructor() {
		super();
		this.state = {
			language: '',
			age: '',
			diabetes: '',
			question1: null,
			question2: null,
			question1Fade: new Animated.Value(1),
			question2Fade: new Animated.Value(0),
			question3: null,
			question3Fade: new Animated.Value(0),
			submitFade: new Animated.Value(0),
			finished: 'false'
		};
	}

	componentDidMount() {
		this.props.props.ui.bottomNavigatorVisible = false;
		let val = AsyncStorage.getItem('demographicsSet').then(() => {
			if (val === 'true') {
				this.setState({ finished: 'true' });
			}
		});
	}

	onSubmit() {
		this.props.props.ui.bottomNavigatorVisible = true;
		AsyncStorage.setItem('language', this.state.language);
		// this.props.userSettings.language = this.state.language;
		AsyncStorage.setItem('age', this.state.age);
		AsyncStorage.setItem('diabetes', this.state.diabetes);
		AsyncStorage.setItem('demographicsSet', 'true');

		//send the data to the database, using the user's unique device id
		setUserSettings(Constants.installationId, this.state.age, this.state.diabetes, this.state.language);

		this.props.loadAgain();
	}

	render() {
		var {height, width} = Dimensions.get('window');
		return (
			<Animated.View style={styles.container}>
				<Animated.View
					style={styles.pickerStyle}
					style={{ opacity: this.state.question1Fade }}
				>
					<Divider style = {{width: width}}/>
					<Subheading style={styles.title}> Choose a language. एक भाषा चुनें। </Subheading>
					<Picker
						mode = "dropdown"
						selectedValue={this.state.language}
						onValueChange={(itemValue, itemIndex) => {
							this.setState({
								language: itemValue,
								question1: true
							});

							Animated.timing(this.state.question2Fade, {
								toValue: 1,
								duration: 1000
							}).start();
							if (!this.state.question1) {
								Animated.timing(this.state.question1Fade, {
									toValue: 0,
									duration: 0
								}).start();
								Animated.timing(this.state.question1Fade, {
									toValue: 1,
									duration: 1000
								}).start();
							}
						}}
					>
						<Picker.Item label="" value="" />
						<Picker.Item label="English" value="en" />
						<Picker.Item label="Hindi" value="hi" />
					</Picker>
				</Animated.View>
				{this.state.question1 != null && (
					<Animated.View
						style={styles.pickerStyle}
						style={{ opacity: this.state.question2Fade }}
					>
						<Divider style = {{width: width}}/>
						<Subheading style={styles.title}> {this.state.language === "en" ? "What is your age?" : "तुम्हारी उम्र क्या हैं?"}</Subheading>
						<Picker
							mode = "dropdown"
							selectedValue={this.state.age}
							onValueChange={(itemValue, itemIndex) => {
								this.setState({
									age: itemValue,
									question2: true
								});
								Animated.timing(this.state.question3Fade, {
									toValue: 1,
									duration: 1000
								}).start();
								if (!this.state.question2) {
									Animated.timing(this.state.question1Fade, {
										toValue: 0,
										duration: 0
									}).start();
									Animated.timing(this.state.question1Fade, {
										toValue: 1,
										duration: 1000
									}).start();
									Animated.timing(this.state.question2Fade, {
										toValue: 0,
										duration: 0
									}).start();
									Animated.timing(this.state.question2Fade, {
										toValue: 1,
										duration: 1000
									}).start();
								}
							}}
						>
							<Picker.Item label="" value="" />
							<Picker.Item label="<18" value="0-17" />
							<Picker.Item label="18-36" value="18-36" />
							<Picker.Item label="37-60" value="37-60" />
							<Picker.Item label=">60" value="61-110" />
						</Picker>
					</Animated.View>
				)}
				{this.state.question1 != null && this.state.question2 != null && (
					<Animated.View
						style={styles.pickerStyle}
						style={{ opacity: this.state.question3Fade }}
					>
						<Divider style = {{width: width}}/>
						<Subheading style={styles.title}>
							{this.state.language === "en" ? " Do you know about diabetes? " :
								"क्या आप मधुमेह के बारे में जानते हैं?"}
						</Subheading>
						<Picker
							mode = "dropdown"
							selectedValue={this.state.diabetes}
							onValueChange={(itemValue, itemIndex) => {
								this.setState({
									diabetes: itemValue,
									question3: true
								});
								Animated.timing(this.state.submitFade, {
									toValue: 1,
									duration: 1000
								}).start();
								if (!this.state.question3) {
									Animated.timing(this.state.question1Fade, {
										toValue: 0,
										duration: 0
									}).start();
									Animated.timing(this.state.question1Fade, {
										toValue: 1,
										duration: 1000
									}).start();
									Animated.timing(this.state.question2Fade, {
										toValue: 0,
										duration: 0
									}).start();
									Animated.timing(this.state.question2Fade, {
										toValue: 1,
										duration: 1000
									}).start();
									Animated.timing(this.state.question3Fade, {
										toValue: 0,
										duration: 0
									}).start();
									Animated.timing(this.state.question3Fade, {
										toValue: 1,
										duration: 1000
									}).start();
								}
							}}
						>
							<Picker.Item label="" value="" />
							<Picker.Item label={this.state.language === "en" ? "Yes" : "हाँ"} value="yes" />
							<Picker.Item label={this.state.language === "en" ? "No" : "नहीं"} value="no" />
						</Picker>
					</Animated.View>
				)}
				{this.state.question1 != null &&
					this.state.question2 != null &&
					this.state.question3 != null && (
						<Animated.View
							style={{ opacity: this.state.submitFade }}
						>
							<Button
								mode = "contained"
								onPress={() => {
									this.onSubmit();
								}}
							>
								{this.state.language === "en" ? "Submit Answers" : "उत्तर जमा करें"}
							</Button>
						</Animated.View>
					)}
			</Animated.View>
		);
	}
}

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
		alignItems: 'center'
	},
	pickerStyle: {
		flex: 1,
		height: 44,
		backgroundColor: '#FFF0E0',
		borderColor: 'black'
	},
	title: {
		fontSize: 18,
		justifyContent: 'center',
		alignItems: 'center'
	}
});

export default inject('userSettings')(observer(HomePageQuestions));
