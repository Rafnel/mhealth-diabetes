/**
 * @name Video2InteractionPage.jsx
 * @author Michael Ibanez
 *
 * @overview Shows the plate game outline
 *
 * @example
 * Used in InteractionsPage
 * RootNavigation.navigate("Game 2")
 */

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { StyleSheet, View, Text, PanResponder, Animated, Easing, Dimensions, Platform, TouchableOpacity, Image, ImageBackground, AsyncStorage} from 'react-native';
import { shuffle, getTimeLeft, move, GAME_STATE } from '../assets/plateSprites/utils';
import {Modal, Header} from '../components/PlateConstants';
import Images from '../assets/plateSprites/food';
import { updateHealthCoins } from '../utils/coinStorage';
import _ from 'lodash';

const GAME_DURATION = 60000 * 5;
let Window = Dimensions.get('window');
let CIRCLE_RADIUS = 12 * Window.scale;

class Draggable extends Component {
  constructor(props) {
	super(props);

	this.state = {
	  pan: new Animated.ValueXY(),
	  scale: new Animated.Value(1),
	  zIndex: 0,
	  backgroundColor: 'white',
	};
  }

  handleOnLayout(event) {
	const { addDropzone } = this.props;
	const { layout } = event.nativeEvent;
	this.layout = layout;
	this.layout.x = this.props.x;
	this.layout.y = this.props.y;
	addDropzone(this, layout);
  }

  UNSAFE_componentWillMount() {
	const { inDropzone, swapItems, index, p, addPoints } = this.props;

	this._panResponder = PanResponder.create({
	  onMoveShouldSetResponderCapture: () => true,
	  onMoveShouldSetPanResponderCapture: () => true,

	  onPanResponderGrant: (e, gestureState) => {
		//console.log('moving', index);
		this.state.pan.setOffset({ x: this.state.pan.x._value, y: this.state.pan.y._value });
		this.state.pan.setValue({ x: 0, y: 0 });

		Animated.spring(this.state.scale, { toValue: 0.75, friction: 3 }).start();

		this.setState({ backgroundColor: 'orange', zIndex: 1 });

	  },

	  onPanResponderMove: Animated.event([null, { dx: this.state.pan.x, dy: this.state.pan.y }]),

	  onPanResponderRelease: (e, gesture) => {
		this.state.pan.flattenOffset();
		Animated.spring(this.state.scale, { toValue: 1 }).start();
		this.setState({ backgroundColor: 'white', zIndex: 0 });

		let dropzone = inDropzone(gesture);
		if (dropzone) {
            console.log(Images[this.props.item1]);
            console.log(this.props.item)
		  console.log('in dropzone', dropzone.index);
          if(dropzone.index == 6){
              if(this.props.item == Images[this.props.item2] || this.props.item == Images[this.props.item3]){
                  addPoints(1, true);
              } else{
                  addPoints(1, false);
              }
          } else if(dropzone.index == 7){
              if(this.props.item == Images[this.props.item1]){
                  addPoints(2, true);
              } else{
                  addPoints(2, false);
              }
          } else if(dropzone.index == 8){
              if(this.props.item == Images[this.props.item2] || this.props.item == Images[this.props.item3]){
                  addPoints(3, true);
              } else{
                  addPoints(3, false);
              }
          }
		  // adjust into place
		  Animated.spring(this.state.pan, { toValue: {
			x: dropzone.x - this.layout.x,
			y: dropzone.y - this.layout.y,
		  } }).start();
		  if (index !== dropzone.index) {
			swapItems(index, dropzone.index);
		  }
		}
		Animated.spring(this.state.pan, { toValue: { x: 0, y: 0 } }).start();
	  }

	});
  }

  render() {
	const { pan, scale, zIndex, backgroundColor } = this.state;
	const [translateX, translateY] = [pan.x, pan.y];
	const rotate = '0deg';
	const imageStyle = {
	  transform: [{ translateX }, { translateY }, { rotate }, { scale }],
	};

	return (
	  <View
		style={[styles.dropzone, { zIndex }]}
		onLayout={event => this.handleOnLayout(event)}
	  >
		<Animated.View
		  {...this._panResponder.panHandlers}
		  style={[imageStyle, styles.draggable, { backgroundColor }]}
		>
		  <Image style={styles.image} source={ this.props.item } />
		</Animated.View>
	  </View>
	);
  }
}

const swap = (array, fromIndex, toIndex) => {
  const newArray = array.slice(0);
  newArray[fromIndex] = array[toIndex];
  newArray[toIndex] = array[fromIndex];
  return newArray;
}

const shuffler = (array) => {
    const newArray = array.slice(0);
    for (let i = array.length - 4; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return newArray;
}

const rand2 = (lim) => {
    return Math.floor(Math.random() * lim) + 1 ;
}

const PlateApp = class PlateApp extends Component {
  constructor(props) {
	super(props);
	this.startGame = this.startGame.bind(this);
	this.resetGame = this.resetGame.bind(this);
	this.gameLoop = this.gameLoop.bind(this);
    this.getScore = this.getScore.bind(this);
    let one = 'veg' + rand2(2);
    let two = 'carb' + rand2(2);
    let thr = 'prot' + rand2(3);

	this.state = {
		items: [
			Images[one],
			Images[two],
			Images[thr],
			Images['other' + rand2(2)],
			Images['other3'],
			Images['other4'],
			Images['blank'],
			Images['blank'],
			Images['blank'],
		],
		dropzones: [],
		dropzoneLayouts: [],
		gameState: GAME_STATE.READY,
		timeLeft: 0,
        correct: [false, false, false],
        corre: 0,
        corrItem1: one,
        corrItem2: two,
        corrItem3: thr,
	};
  }

	startGame = () => {
        let one = 'veg' + rand2(2);
        let two = 'carb' + rand2(2);
        let thr = 'prot' + rand2(3);
		this.setState({
            items: [
    			Images[one],
    			Images[two],
    			Images[thr],
    			Images['other' + rand2(2)],
    			Images['other3'],
    			Images['other4'],
    			Images['blank'],
    			Images['blank'],
    			Images['blank'],
    		],
    		dropzones: [],
    		dropzoneLayouts: [],
    		gameState: GAME_STATE.READY,
    		timeLeft: 0,
            correct: [false, false, false],
            corre: 0,
            corrItem1: one,
            corrItem2: two,
            corrItem3: thr,
		});
		this.currentDeadline = Date.now() + GAME_DURATION;
		shuffler(this.state.items)
		this.setState({
			gameState: GAME_STATE.PLAYING,
			timeLeft: getTimeLeft(this.currentDeadline),
		},
		this.gameLoop
		);
	};

    getScore = () => {
        return this.state.corre;
    }

	gameLoop = () => {
		this.timer = setInterval(() => {
			const timeLeft = getTimeLeft(this.currentDeadline);
			const isTimeout = timeLeft <= 0;
			if (isTimeout && this.timer) {
				clearInterval(this.timer);
			}
			this.setState({
			timeLeft: isTimeout ? 0 : timeLeft,
			...(isTimeout ? { gameState: GAME_STATE.DONE } : {}),
			});
		}, 1000);
	};

	endGame = () => {
        let corr = 0;
        if(this.state.correct[0]){
            corr += 1;
        }
        if(this.state.correct[1]){
            corr += 1;
        }
        if(this.state.correct[2]){
            corr += 1;
        }
		if (this.timer) {
			clearInterval(this.timer);
		}
		this.setState({
			gameState: GAME_STATE.DONE,
            corre: corr,
		});

        this.props.coinState.coins += corr;
        updateHealthCoins(this.props.coinState.coins);
	};

	resetGame = () => {
        let one = 'veg' + rand2(2);
        let two = 'carb' + rand2(2);
        let thr = 'prot' + rand2(3);
		this.setState({
            items: [
    			Images[one],
    			Images[two],
    			Images[thr],
    			Images['other' + rand2(2)],
    			Images['other3'],
    			Images['other4'],
    			Images['blank'],
    			Images['blank'],
    			Images['blank'],
    		],
    		dropzones: [],
    		dropzoneLayouts: [],
    		gameState: GAME_STATE.READY,
    		timeLeft: 0,
            correct: [false, false, false],
            corre: 0,
            corrItem1: one,
            corrItem2: two,
            corrItem3: thr,
		});
	};

  addDropzone(dropzone, dropzoneLayout) {
	const { items, dropzones, dropzoneLayouts } = this.state;
	// HACK: to make sure setting state does not re-add dropzones
	if (items.length !== dropzones.length) {
	  this.setState({
		dropzones: [...dropzones, dropzone],
		dropzoneLayouts: [...dropzoneLayouts, dropzoneLayout],
	  });
	}
  }

  inDropzone(gesture) {
	const { dropzoneLayouts } = this.state;
	// HACK: with the way they are added, sometimes the layouts end up out of order, so we need to sort by y,x (x,y doesn't work)
	const sortedDropzoneLayouts = _.sortBy(dropzoneLayouts, ['y', 'x']);
	let inDropzone = false;

	sortedDropzoneLayouts.forEach((dropzone, index) => {
		const inX = gesture.moveX > dropzone.x && gesture.moveX < dropzone.x + dropzone.width;
		const inY = gesture.moveY > dropzone.y && gesture.moveY < dropzone.y + dropzone.height;
		if (inX && inY) {
			inDropzone = dropzone;
			inDropzone.index = index;
		}
		});
		return inDropzone;
  }

  swapItems(fromIndex, toIndex) {
    	console.log('swapping', fromIndex, '<->', toIndex);
    	const { items, dropzones } = this.state;
    	this.setState({
    	  items: swap(items, fromIndex, toIndex),
    	  dropzones: swap(dropzones, fromIndex, toIndex),
    	});
      }

    addPoints = (num, val) =>{
        let check = this.state.correct;
        if(num == 1){
            check[0] = val;
        } else if( num == 2){
            check[1] = val;
        } else if( num == 3){
            check[2] = val;
        }
        this.setState({
            correct: check,
        });
    }

  render() {
	const { showDraggable, dropZoneValues, gameState, timeLeft, bench, ...groups } = this.state;
		const isDropDisabled = gameState === GAME_STATE.DONE;
		return (
			<View style={styles.container}>
				<Header gameState={gameState} timeLeft={timeLeft} endGame={this.endGame} />
					{this.state.gameState !== GAME_STATE.PLAYING && (
					  <Modal
						startGame={this.startGame}
						resetGame={this.resetGame}
						timeLeft={timeLeft}
						gameState={gameState}
						getScore={this.getScore}
                        item1={this.state.corrItem1}
                        item2={this.state.corrItem2}
                        item3={this.state.corrItem3}
					  />
					)}
					{(this.state.gameState === GAME_STATE.PLAYING) && (
					<View style={styles.containerIn}>
						<Draggable
							key={0}
							index={0}
							p={false}
							x={1}
							y={1}
						  item={this.state.items[0]}
						  addDropzone={this.addDropzone.bind(this)}
						  inDropzone={this.inDropzone.bind(this)}
						  swapItems={this.swapItems.bind(this)}
                          addPoints={this.addPoints.bind(this)}
                          item1={this.state.corrItem1}
                          item2={this.state.corrItem2}
                          item3={this.state.corrItem3}
						/>
						<Draggable
							key={1}
							index={1}
							p={false}
							x={2}
							y={2}
						  item={this.state.items[1]}
						  addDropzone={this.addDropzone.bind(this)}
						  inDropzone={this.inDropzone.bind(this)}
						  swapItems={this.swapItems.bind(this)}
                          addPoints={this.addPoints.bind(this)}
                          item1={this.state.corrItem1}
                          item2={this.state.corrItem2}
                          item3={this.state.corrItem3}
						/>
						<Draggable
							key={2}
							index={2}
							p={false}
							x={3}
							y={3}
						  item={this.state.items[2]}
						  addDropzone={this.addDropzone.bind(this)}
						  inDropzone={this.inDropzone.bind(this)}
						  swapItems={this.swapItems.bind(this)}
                          addPoints={this.addPoints.bind(this)}
                          item1={this.state.corrItem1}
                          item2={this.state.corrItem2}
                          item3={this.state.corrItem3}
						/>
						<Draggable
							key={3}
							index={3}
							p={false}
							x={4}
							y={4}
						  item={this.state.items[3]}
						  addDropzone={this.addDropzone.bind(this)}
						  inDropzone={this.inDropzone.bind(this)}
						  swapItems={this.swapItems.bind(this)}
                          addPoints={this.addPoints.bind(this)}
                          item1={this.state.corrItem1}
                          item2={this.state.corrItem2}
                          item3={this.state.corrItem3}
						/>
						<Draggable
							key={4}
							index={4}
							p={false}
							x={5}
							y={5}
						  item={this.state.items[4]}
						  addDropzone={this.addDropzone.bind(this)}
						  inDropzone={this.inDropzone.bind(this)}
						  swapItems={this.swapItems.bind(this)}
                          addPoints={this.addPoints.bind(this)}
                          item1={this.state.corrItem1}
                          item2={this.state.corrItem2}
                          item3={this.state.corrItem3}
						/>
						<Draggable
							key={5}
							index={5}
							p={false}
							x={6}
							y={6}
						  item={this.state.items[5]}
						  addDropzone={this.addDropzone.bind(this)}
						  inDropzone={this.inDropzone.bind(this)}
						  swapItems={this.swapItems.bind(this)}
                          addPoints={this.addPoints.bind(this)}
                          item1={this.state.corrItem1}
                          item2={this.state.corrItem2}
                          item3={this.state.corrItem3}
						/>
						<View style={styles.containedImage}>
							<ImageBackground source={require('../assets/plateSprites/plate.png')} style={styles.backgroundImage}>
							<View style={styles.plateDrop1}>
								<Draggable
									key={6}
									index={6}
									p={true}
									x={100}
									y={500}
								  item={this.state.items[6]}
								  addDropzone={this.addDropzone.bind(this)}
								  inDropzone={this.inDropzone.bind(this)}
								  swapItems={this.swapItems.bind(this)}
                                  addPoints={this.addPoints.bind(this)}
                                  item1={this.state.corrItem1}
                                  item2={this.state.corrItem2}
                                  item3={this.state.corrItem3}
								/>
							</View>
							<View style={styles.plateDrop2}>
								<Draggable
									key={7}
									index={7}
									p={true}
									x={230}
									y={575}
								  item={this.state.items[7]}
								  addDropzone={this.addDropzone.bind(this)}
								  inDropzone={this.inDropzone.bind(this)}
								  swapItems={this.swapItems.bind(this)}
                                  addPoints={this.addPoints.bind(this)}
                                  item1={this.state.corrItem1}
                                  item2={this.state.corrItem2}
                                  item3={this.state.corrItem3}
								/>
						</View>
							<View style={styles.plateDrop3}>
								<Draggable
									key={8}
									index={8}
									p={true}
									x={100}
									y={650}
								  item={this.state.items[8]}
								  addDropzone={this.addDropzone.bind(this)}
								  inDropzone={this.inDropzone.bind(this)}
								  swapItems={this.swapItems.bind(this)}
                                  addPoints={this.addPoints.bind(this)}
                                  item1={this.state.corrItem1}
                                  item2={this.state.corrItem2}
                                  item3={this.state.corrItem3}
								/>
						</View>
							</ImageBackground>
						</View>
					</View>
					)}

			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
	paddingTop: 30,
	backgroundColor: 'orange',
	justifyContent: 'center',
	alignItems: 'center',
  },
  containerIn: {
	paddingTop: 30,
	backgroundColor: 'orange',
	justifyContent: 'center',
	alignItems: 'center',
	flexDirection: 'row',
	flexWrap: 'wrap',
  },
  dropzone: {
	zIndex: -1,
	width: 106,
	height: 106,
	borderColor: 'green',
	borderWidth: 3,
	backgroundColor: 'lightgreen',
  },
  draggable: {
	backgroundColor: 'white',
	justifyContent: 'center',
	alignItems: 'center',
	width: 100,
	height: 100,
	borderWidth: 1,
	borderColor: 'black'
  },
  backgroundImage: {
	  height: 400,
	  width : 400,
	 resizeMode: 'contain'
  },
  containedImage: {
	  alignItems: 'center',
	  justifyContent: 'center',
	  height: Window.height *0.50,
	  width: Window.width * 0.80,
  },
  plateDrop1 :{
	  position: 'absolute',
	  left: 85,
	  top: 70,
  },
  plateDrop2 :{
	  position: 'absolute',
	  left: 220,
	  top: 150,
  },
  plateDrop3 :{
	  position: 'absolute',
	  left: 85,
	  top: 215,
  },
  image: {
	width: 75,
	height: 75
  }
});

export default inject('ui', 'userSettings', 'coinState',)(observer(PlateApp));
