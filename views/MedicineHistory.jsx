import { Text, View, ScrollView } from "react-native"
import React from 'react';
import { inject, observer } from "mobx-react";
import { Surface, Divider } from "react-native-paper";
import MedicineDatabase from "../utils/medicineDB";


const MedicineHistory = class MedicineHistory extends React.Component {
    returnAllMedication = () => {
        console.log(JSON.stringify(this.props.ui.allMedication))
        let toReturn = [];
        
        for(let i = 0; i < this.props.ui.allMedication.length; i++) {
            let medicine = this.props.ui.allMedication[i];
            if(this.props.userSettings.language === "en") {
                //change type of dosage
                let dosage;
                switch(medicine.dosage){
                    case "OnceDaily" : dosage = "Once Daily";
                                  break;
                    case "TwiceDaily" : dosage = "Twice Daily";
                                     break;
                    case "ThreeDaily" : dosage = "Three Times Daily";
                                     break;
                    case "OnceWeekly" : dosage = "Once Weekly";
                                      break;
                    case "TwiceWeekly" : dosage = "Twice Weekly";
                                   break;
                    case "Other" : dosage = "Other";
                                   break;
                }
                toReturn.push(<Text key = {i}>{medicine.date + ": " + medicine.name + ", " + dosage}</Text>);
            } else {
                //need to change the date to dd/mm/yyyy since that is common in hindi
                String.prototype.replaceAt=function(index, replacement) {
                    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
                }
                let date = medicine.date;
                let month = date.substr(0, 2);
                let day = date.substr(3, 2);
                date = date.replaceAt(0, day.charAt(0));
                date = date.replaceAt(1, day.charAt(1));
                date = date.replaceAt(3, month.charAt(0));
                date = date.replaceAt(4, month.charAt(1));
                //change type of dosage
                let dosage;
                switch(medicine.dosage){
                    case "OnceDaily" : dosage = "एक बार रोज़";
                                  break;
                    case "TwiceDaily" : dosage = "दिन में दो बार";
                                     break;
                    case "ThreeDaily" : dosage = "तीन बार दैनिक";
                                     break;
                    case "OnceWeekly" : dosage = "एक बार साप्ताहिक";
                                      break;
                    case "TwiceWeekly" : dosage = "सप्ताह में दो बार";
                                   break;
                    case "Other" : dosage = "अन्य";
                                   break;
                }
                
                toReturn.push(<Text key = {i}>{date + ": " + medicine.name + ", " + dosage}</Text>);
            }
        }

        return toReturn;
    }
    render() {
        let lang = this.props.userSettings.language;
        return(
            <ScrollView>
                <Surface style = {{elevation: 3, padding: 10, marginTop: 10}}>
                    <Text>{lang === "en" ? "All of your medication from most recent to least recent are available here." : "आपकी सभी दवाएँ सबसे हाल ही में कम से कम हाल ही में यहाँ उपलब्ध हैं।"}</Text>
                    <Divider style = {{width: 600}}/>
                    {this.returnAllMedication()}

                </Surface>

                <Text style = {{height: 50}}/>
            </ScrollView>
        )
    }

    async componentDidMount() {
        let medicineDBService = new MedicineDatabase();
        medicineDBService.initDB();
        let results = await medicineDBService.executeSQL("select * from medicineDB", []);
        this.props.ui.allMedication = results.rows._array;

        const medicationCopy = [...this.props.ui.allMedication];
        medicationCopy.sort((medicineA, medicineB) => medicineA.date > medicineB.date ? -1 : 1)
        this.props.ui.allMedication = medicationCopy;
    }
}

export default inject('ui', 'userSettings')(observer(MedicineHistory));
