/**
 * @name FoodHistory
 * @author Mario Arturo Lopez Martinez
 * @overview Page that allows you to see previous logs made of food.
 * @example Used in LoggingPage <FoodHistory />
 */

import React from 'react';
import FoodDatabase from '../utils/foodDB';
import { Surface, Divider, Card, Title } from 'react-native-paper';
import { Text, ScrollView } from 'react-native';
import { inject, observer } from 'mobx-react';

const FoodHistory = class FoodHistory extends React.Component {
	async componentDidMount() {
		let foodDBService = new FoodDatabase();
		foodDBService.initDB();
		let results = await foodDBService.executeSQL('select * from foodDB', []);
		this.props.ui.allFood = results.rows._array;
		const foodCopy = [...this.props.ui.allFood];
		foodCopy.sort((foodA, foodB) => (foodA.timestamp > foodB.timestamp ? -1 : 1));
		this.props.ui.allFood = foodCopy;
	}

	returnAllFood = () => {
		let toReturn = [];

		for (let i = 0; i < this.props.ui.allFood.length; i++) {
			let food = this.props.ui.allFood[i];
			console.log(food.uri);
			if (this.props.userSettings.language === 'en') {
				toReturn.push(
					<React.Fragment key={i}>
						<Card>
							<Card.Title title={food.date} subtitle={food.description} />
							<Card.Content>
								<Title>{!food.isHealthy ? 'Healthy food! :^)' : 'NOT Healthy! :^('}</Title>
							</Card.Content>
							<Card.Cover source={{ uri: food.uri }} />
						</Card>
					</React.Fragment>
				);
			} else {
				toReturn.push(
					<React.Fragment key={i}>
						<Card>
							<Card.Title title={food.date} subtitle={food.description} />
							<Card.Content>
								<Title>{food.isHealthy ? 'स्वस्थ भोजन! :^)' : 'स्वस्थ नहीं! :^('}</Title>
							</Card.Content>
							<Card.Cover source={{ uri: food.uri }} />
						</Card>
					</React.Fragment>
				);
			}
		}
		return toReturn;
	};

	render() {
		let lang = this.props.userSettings.language;
		return (
			<ScrollView>
				<Surface style={{ elevation: 3, padding: 10, marginTop: 10 }}>
					<Text>
						{lang === 'en'
							? 'All of your food logs from most recent to least recent are available here.'
							: 'आपके सभी वर्कआउट सबसे हाल ही में कम से कम हाल ही में यहां उपलब्ध हैं।'}
					</Text>
					<Divider style={{ width: 600 }} />
					{this.returnAllFood()}
				</Surface>
				<Text style={{ height: 50 }} />
			</ScrollView>
		);
	}
};

export default inject('ui', 'userSettings')(observer(FoodHistory));
