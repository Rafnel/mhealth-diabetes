/**
 * @name Video4Page
 * @author Andrew Case
 *
 * @overview Page containing embedded video 4.
 *
 * @example
 * Used in InteractionsPage
 * () => RootNavigation.navigate(lang === "en" ? "Video 4" : "वीडियो 4")
 */

import React from 'react';
import { Text, View, AsyncStorage } from 'react-native';
import { mainStyle } from './Styling';
import { EmbeddedVideo } from './EmbeddedVideo';
import { inject, observer } from 'mobx-react';
import { Button, Title } from 'react-native-paper';
import { updateVideosState } from '../utils/videoProgressStorage';
import * as RootNavigation from "../state/RootNavigation";
import { updateHealthCoins } from '../utils/coinStorage';

const Video4Page = class Video4Page extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			videoPlaying: false,
			languageSetting: 'hi',
			currentPosition: 0,
			finishedPlaying: false
		};
		this.getLanguage();
		this.handler = this.handler.bind(this);
	}

	// Sets the language based on their preferences
	getLanguage = async () => {
		try {
			let value = await AsyncStorage.getItem('language')
			this.setState({ languageSetting: value });
		} catch (error) {
			console.log("ERROR")
		}
	};

	// This is the function passed to the EmbeddedVideo component
	handler(currentPosition) {
		// If the video is finished
		if (currentPosition == "Finished") {
			this.props.notifications.setSuccessNotification(
				this.state.languageSetting === "en" ? 'Congrats on finishing the video! Gained 4 Health Coins' : "वीडियो को पूरा करने के लिए बधाई! 4 स्वास्थ्य सिक्का दिया"
			)
			videos = this.props.videos;
			// If the video is the current video
			if (videos.currentVideo == 4) {
				videos.currentVideo = 5;
				videos.currentVideoProgress = 0;
				updateVideosState(videos);
			}
			RootNavigation.navigate("Interactions");
			this.props.coinState.coins += 4;
			updateHealthCoins(this.props.coinState.coins);
		} else {
			videos = this.props.videos;
			if (videos.currentVideo == 4) {
				videos.currentVideoProgress = currentPosition;
				updateVideosState(videos);
			}
		}
	}

	componentDidMount() {
		// This will pause the video if the view is left
		this.props.navigation.addListener('didBlur', route => {
			this.setState({ videoPlaying: !this.state.videoPlaying });
		});
		// Sets the current position in the video upon the page being rendered
		videos = this.props.videos;
		if (videos.currentVideo == 4) {
			this.setState({ currentPosition: videos.currentVideoProgress });
		}
	}

	render() {
		return (
			<View style={mainStyle.container}>
				<Title style={{ textAlign: 'center' }}> {this.state.languageSetting === "en" ? "Preventing and Managing Diabetes" : "मधुमेह की रोकथाम और प्रबंधन"} </Title>
				{this.state.languageSetting == 'en' ? (
					<EmbeddedVideo
						path={require('../videos/mHealth-Diabetes-Part-4-English.mp4')}
						videoPlaying={this.state.videoPlaying}
						currentPosition={this.state.currentPosition}
						finishedPlaying={this.handler}
					/>
				) : (
						<EmbeddedVideo
							path={require('../videos/mHealth-Diabetes-Part-4-Hindi.mp4')}
							videoPlaying={this.state.videoPlaying}
							currentPosition={this.state.currentPosition}
							finishedPlaying={this.handler}
						/>
					)}
			</View>
		);
	}
};

export default inject('notifications', 'userSettings', 'videos', 'coinState')(observer(Video4Page));
