/**
 * @name InteractionsPage
 * @author Andrew Case
 *
 * @overview Page containing all the educational material (videos/games).
 *
 * @example
 * Used in CustomTabBar for navigation.
 * RootNavigation.navigate("Interactions");
 */

import React from 'react';
import { Text, View, AsyncStorage, Image, ScrollView } from 'react-native';
import { List, Surface } from 'react-native-paper';
import { getVideosObject } from '../utils/videoProgressStorage';
import * as RootNavigation from "../state/RootNavigation";
import { inject, observer } from 'mobx-react';
import { currentVideoMessage, getVideoProgress } from '../utils/language';

const InteractionsPage = class InteractionsPage extends React.Component {
	// Upon reaching the interactions page, update the video state store
	async componentDidMount() {
		let currentVideoState = await getVideosObject();
		this.props.userSettings.language = await AsyncStorage.getItem("language");
		this.props.videos.currentVideo = currentVideoState.currentVideo;
		this.props.videos.currentVideoProgress = currentVideoState.currentVideoProgress;
	}

	returnExplanation = () => {
		let explanation = "";
		if(this.props.userSettings.language === "en"){
			explanation = "On this page, you can watch educational videos to learn more about diabetes and unlock rewards and games!";
		}
		else{
			explanation = "इस पृष्ठ पर, आप मधुमेह के बारे में अधिक जानने और पुरस्कार और खेलों को अनलॉक करने के लिए शैक्षिक वीडियो देख सकते हैं!";
		}

		return <Text>{explanation}</Text>;
	}

	render() {
		let lang = this.props.userSettings.language;
		return (
			<ScrollView>
				<Surface style={{ elevation: 3, padding: 10 }}>
					<View style={{justifyContent: 'center', alignItems: 'center'}}>
						<Image style = {{width: 200, height: 200}} resizeMode = "contain" source = {require('../assets/Videos_Monkey.png')}/>
					</View>
					{this.returnExplanation()}
					<Text/>
					<Text>{currentVideoMessage(lang, this.props.videos.currentVideo)}</Text>
					{this.props.videos.currentVideo === 5 ?
						null :
						<Text>{getVideoProgress(lang, this.props.videos.currentVideoProgress)}</Text>
					}
				</Surface>
				<List.Section style={{ width: 350 }}>
					<List.Subheader>{lang === "en" ? "Videos" : "वीडियो"}</List.Subheader>
					<List.Item
						left={props => <List.Icon {...props} icon={this.iconChecker(1)} />}
						title={lang === "en" ? "Introduction to Diabetes" : "मधुमेह का परिचय"}
						onPress={this.iconChecker(1) === "lock" ? null : () => RootNavigation.navigate(lang === "en" ? "Video 1" : "वीडियो 1")}
					/>
					<List.Item
						left={props => <List.Icon {...props} icon={this.iconChecker(2)} />}
						title={lang === "en" ? "Symptoms of Diabetes" : "मधुमेह के लक्षण"}
						onPress={this.iconChecker(2) === "lock" ?
							() => this.props.notifications.setErrorNotification(lang === "en" ? "Watch previous videos to unlock this one!" : "इसे अनलॉक करने के लिए पिछले वीडियो देखें!") :
							() => RootNavigation.navigate(lang === "en" ? "Video 2" : "वीडियो 2")}
					/>
					<List.Item
						left={props => <List.Icon {...props} icon={this.iconChecker(3)} />}
						title={lang === "en" ? "Preventing and Managing Diabetes" : "मधुमेह की रोकथाम और प्रबंधन"}
						onPress={this.iconChecker(3) === "lock" ?
							() => this.props.notifications.setErrorNotification(lang === "en" ? "Watch previous videos to unlock this one!" : "इसे अनलॉक करने के लिए पिछले वीडियो देखें!") :
							() => RootNavigation.navigate(lang === "en" ? "Video 3" : "वीडियो 3")}
					/>
					<List.Item
						left={props => <List.Icon {...props} icon={this.iconChecker(4)} />}
						title={lang === "en" ? "Good Eating Habits" : "गुड खाने की आदत"}
						onPress={this.iconChecker(4) === "lock" ?
							() => this.props.notifications.setErrorNotification(lang === "en" ? "Watch previous videos to unlock this one!" : "इसे अनलॉक करने के लिए पिछले वीडियो देखें!") :
							() => RootNavigation.navigate(lang === "en" ? "Video 4" : "वीडियो 4")}
					/>
				</List.Section>

				<List.Section style={{ width: 350 }}>
					<List.Subheader>{lang === "en" ? "Games" : "खेल"}</List.Subheader>
					<List.Item
						left={props => <List.Icon {...props} icon={this.iconChecker(2) === "lock" ? "lock" : "xbox-controller"} />}
						title={lang === "en" ? "Snake Game" : "साँप का खेल"}
						onPress={this.iconChecker(2) === "lock" ?
							() => this.props.notifications.setErrorNotification(lang === "en" ? "Watch 1st video to unlock this one!" : "इसे अनलॉक करने के लिए पिछले वीडियो देखें!") :
							() => RootNavigation.navigate(lang === "en" ? "Game 1" : "खेल 1")}
					/>
					<List.Item
						left={props => <List.Icon {...props} icon={this.iconChecker(2) === "lock" ? "lock" : "xbox-controller"} />}
						title={lang === "en" ? "Plate Game" : "साँप का खेल"}
						onPress={this.iconChecker(2) === "lock" ?
							() => this.props.notifications.setErrorNotification(lang === "en" ? "Watch 2nd video to unlock this one!" : "इसे अनलॉक करने के लिए पिछले वीडियो देखें!")  :
							() => RootNavigation.navigate(lang === "en" ? "Game 2" : "खेल 2")}
					/>
				</List.Section>

				<Text style = {{height: 50}}/>
			</ScrollView>
		);
	}

	// Will put the correct icon on the corresponding video, either a lock, check-mark, or play button
	iconChecker(videoNumber) {
		if (this.props.videos.currentVideo == videoNumber) {
			return "play-circle";
		} else if (this.props.videos.currentVideo > videoNumber) {
			return "check";
		} else {
			return "lock";
		}
	}
}

export default inject('notifications', 'videos', 'ui', 'userSettings')(observer(InteractionsPage));
