const images = [
    require('./mustache.png'),
    require('./tophat.png'),
    require('./glasses.png'),
];

export default images;