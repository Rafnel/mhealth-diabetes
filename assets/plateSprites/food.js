/**
 * @name food.js
 * @author Michael Ibanez
 *
 * @overview The images that the plate game uses
 *
 * @example
 * Used in InteractionsPage
 * Images[blank]
 */
 export default Images = {
    blank: require('./blank.png'),
    veg1: require('./okra.png'),
    veg2: require('./carrot.png'),
    carb1: require('./rice.png'),
    carb2: require('./redmillet.png'),
    prot1: require('./cheese.png'),
    prot2: require('./some.png'),
    prot3: require('./egg.png'),
    other1: require('./wine.png'),
    other2: require('./junk_1.png'),
    other3: require('./junk_2.png'),
    other4: require('./junk_3.png'),
}

// Half plate vegetable, quarter protein, quarter carbs
