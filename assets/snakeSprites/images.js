/**
 * @name images.js
 * @author Michael Ibanez
 *
 * @overview The images that the snake game uses
 *
 * @example
 * Used in InteractionsPage
 * Images[mon1]
 */

 export default Images = {
    mon1: require('./down.png'),
    mon2: require('./left.png'),
    mon3: require('./up.png'),
    mon4: require('./right.png'),
    fruit1: require('./apple.png'),
    fruit2: require('./banana.png'),
    fruit3: require('./cherry.png'),
    fruit4: require('./orange.png'),
    fruit5: require('./peach.png'),
    fruit6: require('./pear.png'),
    fruit7: require('./strawberry.png'),
    fruit8: require('./watermelon.png'),
}
