/**
 * @name Store
 * @author Andrew Case
 *
 * @overview Store containing the images/items to be bought.
 *
 * @example
 * Used in HomePage
 * <Store />
 */

import React from 'react';
import { Text, View, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { Surface, Subheading } from 'react-native-paper';
import { observer, inject } from 'mobx-react';
import { updateOwnedItems, addItemToOwned } from '../utils/storeItemsStorage';
import Images from '../assets/avatarItems/imageConstants';
import { storeImageStyle } from '../views/Styling';

const Store = class Store extends React.Component {
	state = {
		lang: this.props.userSettings.language,
		items: this.props.storeItems,
	};

	spendCoins(itemName, cost) {
		let message =
			this.state.lang === 'en'
				? 'You purchased ' + itemName + ' for ' + cost + ' health coin'
				: 'आपने खरीदा ' + itemName + ' के लिये ' + cost;
		// Change plural of coins if more than one coin is spent
		if (cost > 1) {
			this.state.lang === 'en' ? (message += 's') : (message += ' स्वास्थ्य के सिक्के');
		} else if (this.state.lang === 'hi') {
			message += ' स्वास्थ्य सिक्का';
		}
		message += '!';
		// If they don't have enough money
		if (this.props.coinState.coins - cost < 0) {
			this.props.notifications.setErrorNotification(
				this.state.lang === 'en'
					? 'You do not have enough coins to purchase that!'
					: 'आपके पास खरीदने के लिए पर्याप्त सिक्के नहीं हैं!'
			);
		} else if (this.state.items.owned.indexOf(itemName) !== -1) {
			this.props.notifications.setErrorNotification(
				this.state.lang === 'en' ? 'You already own this item!' : 'आप पहले से ही इस वस्तु के स्वामी हैं!'
			);
		} else {
			this.props.coinState.coins -= cost;
			// If value doesn't exists (aka they don't own it)
			if (this.state.items.owned.indexOf(itemName) === -1) {
				this.state.items.owned.push(itemName);
				addItemToOwned(itemName).then(() => this.props.notifications.setSuccessNotification(message));
			}
		}
	}

	renderStoreItems() {
		let storeItemList = [];
		if (this.state.lang === 'en') {
			this.state.items.storeItems.forEach((tempItem, i) => {
				storeItemList.push(
					<TouchableOpacity
						key={tempItem}
						style={{ paddingHorizontal: 20 }}
						onPress={() => this.spendCoins(tempItem, this.state.items.storeItemCosts[i])}
					>
						<Text>{tempItem.charAt(0).toUpperCase() + tempItem.substring(1)}</Text>
						<Text style={{ color: 'red' }}>Cost: ${this.state.items.storeItemCosts[i]}</Text>
						{/* <IconButton icon = "coin" color = "#000000" style={{marginTop:0, marginLeft:0}} size={20} /> */}
						<ImageBackground style={storeImageStyle.image} resizeMode="contain" source={Images[i]}>
							{/* If they already own the item, add a checkmark to the image */}
							{this.state.items.owned.indexOf(tempItem) !== -1 ? (
								<Image
									style={storeImageStyle.alreadyOwned}
									resizeMode="center"
									source={require('../assets/avatarItems/checkmark.png')}
								/>
							) : null}
						</ImageBackground>
					</TouchableOpacity>
				);
			});
		} else {
			this.state.items.storeItemsHindi.forEach((tempItem, i) => {
				storeItemList.push(
					<TouchableOpacity
						key={tempItem}
						style={{ paddingHorizontal: 20 }}
						onPress={() => this.spendCoins(tempItem, this.state.items.storeItemCosts[i])}
					>
						<Text>{tempItem.charAt(0).toUpperCase() + tempItem.substring(1)}</Text>
						<Text style={{ color: 'red' }}>
							{this.state.lang === 'en' ? 'Cost: $' : 'लागत: ₹'}
							{this.state.items.storeItemCosts[i]}
						</Text>
						{/* <IconButton icon = "coin" color = "#000000" style={{marginTop:0, marginLeft:0}} size={20} /> */}
						<ImageBackground style={storeImageStyle.image} resizeMode="contain" source={Images[i]}>
							{/* If they already own the item, add a checkmark to the image */}
							{this.state.items.owned.indexOf(tempItem) !== -1 ? (
								<Image
									style={storeImageStyle.alreadyOwned}
									resizeMode="center"
									source={require('../assets/avatarItems/checkmark.png')}
								/>
							) : null}
						</ImageBackground>
					</TouchableOpacity>
				);
			});
		}

		return storeItemList;
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				<Surface style={{ elevation: 3, padding: 10, marginTop: 25 }}>
					<Subheading>{this.state.lang === 'en' ? 'Pet Store' : 'वर्कआउट लॉगिंग'}</Subheading>
					<View style={{ flexDirection: 'row', justifyContent: 'space-evenly', flexWrap: 'wrap' }}>
						{this.renderStoreItems()}
					</View>
				</Surface>
			</View>
		);
	}

	async componentDidMount() {
		await updateOwnedItems(this.state.items);
		this.renderStoreItems();
	}
};

export default inject('ui', 'userSettings', 'coinState', 'notifications', 'storeItems')(observer(Store));
