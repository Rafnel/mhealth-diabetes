/**
 * @name PlateConstants.jsx
 * @author Michael Ibanez
 *
 * @overview Contains components that the plate game uses
 *
 * @example
 * Used in Video2InteractionPage
 * <Header> </Header>
 * <Modal> </Modal>
 */

 import React, { Component } from "react";
import { GAME_STATE, getSeconds, getTotalScore } from '../assets/plateSprites/utils';
import { SafeAreaView, View, Text, Button, StyleSheet, PanResponder, Animated, Alert, Image, ImageBackground, Dimensions} from "react-native";

//////////////////////////////////////////////
let Window = Dimensions.get('window');

class Header extends Component{
	constructor(props) {
		super(props);
		this.endGame = this.endGame.bind(this);
	}

	endGame = () => {
		this.props.endGame();
	};

	render(){
		return(
			<View className="navbar">
				{this.props.gameState === GAME_STATE.PLAYING && (
					<View>
						<View className="navbar-center">
							<Button className="btn btn-default" title="Check" style={{zIndex:2}} onPress={this.endGame}>
							</Button>
						</View>
					</View>
				)}
			</View>
		)
	}
};

//////////////////////////////////////////////


class Modal extends Component{
	constructor(props) {
		super(props);
		this.startGame = this.startGame.bind(this);
		this.resetGame = this.resetGame.bind(this);
		this.getScore = this.getScore.bind(this);
	}

	startGame = () => {
		this.props.startGame();
	};

	resetGame = () => {
        this.props.startGame();
	}

	getScore = () => {
		let num = this.props.getScore();
		const bonus = getSeconds(this.props.timeLeft)/100;
		let total = num? Math.floor(num + bonus) : 0;
		return `${total}\n You got ${num} correct answers! \nCheck out the solution below!`;
	}

	render(){
		return(
		  <View className='modal modal-sm active'>
			<View className="modal-overlay" />
			<View className="modal-container" style={styles.full}>
			  <View className="modal-header">
				<View className="modal-title h4"><Text >Plate Game </Text></View>
			  </View>
			  <View className="modal-body">
				<View className="content h6">
				  {this.props.gameState === GAME_STATE.READY
					? <View>
                        <Text> Use the My Plate method of 1/2 vegetables, </Text>
                        <Text> 1/4 protein, and 1/4  carbs to drag and  </Text>
                        <Text> drop the food in their correct place. </Text>
                    </View>
					: <View>
							<Text >Coins earned - ${this.getScore()} </Text>
						</View>}
				</View>
			  </View>
			  <View className="modal-footer">
				<Button title={this.props.gameState === GAME_STATE.READY ? 'Start new game': 'Try Again?'}
				  className="btn btn-primary"
				  onPress={this.props.gameState === GAME_STATE.READY ? this.startGame : this.resetGame}
				  style={styles.full}
				>
				</Button>
				{this.props.gameState === GAME_STATE.READY &&
					<View>
						<Image source={require('../assets/plateSprites/howTo.gif')} style={styles.gif}/>
					</View>}
				{this.props.gameState !== GAME_STATE.READY &&
					<View>
						<ImageBackground source={require('../assets/plateSprites/plate.png')} style={styles.backgroundImage}>
						</ImageBackground>
						<Image source={Images[this.props.item1]} style={styles.plateDrop2}></Image>
						<Image source={Images[this.props.item2]} style={styles.plateDrop1}></Image>
						<Image source={Images[this.props.item3]} style={styles.plateDrop3}></Image>
					</View>}
			  </View>
			</View>
		  </View>
		)
	}
};
const styles = StyleSheet.create({
	container: {
	  backgroundColor: "#f2f2f2"
	},
	backgroundImage: {
		zIndex: -1,
  	  height: 400,
  	  width : 400,
  	 resizeMode: 'contain',
	},
	dropContainer: {
	  flex: 1,
	  borderBottomWidth: 2,
	  borderBottomColor: "black",
	  backgroundColor: "white",
	  height: 300
	},
	dragContainer: {
	  flex: 1,
	  width: 30,
	  backgroundColor: "red",
	  alignItems: "center",
	  justifyContent: "space-around",
	  flexDirection: "row"
	},
	full: {
		backgroundColor: 'orange',
		height: Window.height * 0.9,
		width: Window.width,
		alignItems: "center",
	},
	plateDrop1 :{
		zIndex: 10000,
  	  position: 'absolute',
  	  left: 85,
  	  top: 70,
	  width: 106,
  	height: 106,
	},
	plateDrop2 :{
  	  position: 'absolute',
  	  left: 220,
  	  top: 150,
	  width: 106,
  	height: 106,
	},
	plateDrop3 :{
  	  position: 'absolute',
  	  left: 85,
  	  top: 215,
	  width: 106,
  	   height: 106,
	},
	gif :{
		height: 450,
		width : 450,
		resizeMode: 'contain',
	},
  });

export { Header, Modal };
