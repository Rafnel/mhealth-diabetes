/**
 * @name CustomTabBar
 * @author Zac Steudel
 *
 * @overview This component is the tab bar at the bottom of the screen, using
 * the react-native-tabbar-bottom component. React-Navigation's tabbar did not allow
 * the level of customization desired, while also being too complicated to tool with.
 *
 */

import React from 'react';
import { inject, observer } from 'mobx-react';
import Tabbar from 'react-native-tabbar-bottom';
import { View } from 'react-native';
import * as RootNavigation from '../state/RootNavigation';

const CustomTabBar = class CustomTabBar extends React.Component{
    render(){
        let lang = this.props.userSettings.language;
        if(this.props.ui.bottomNavigatorVisible){
            return(
                <View>
                    <Tabbar
                        stateFunc={(tab) => {
                            this.props.ui.page = tab.page;
                            RootNavigation.navigate(tab.page);
                        }}
                        activePage = {this.props.ui.page}
                        tabs = {[
                        {
                            page: lang === "en" ? "Interactions" : "सहभागिता",
                            icon: "clipboard",
                        },
                        {
                            page: lang === "en" ? "Home" : "घर",
                            icon: "home",
                        },
                        {
                            page: lang === "en" ? "Logging" : "लॉगिंग",
                            icon: "book",
                        },
                        {
                            page: lang === "en" ? "Settings" : "समायोजन",
                            icon: "settings",
                        }]}
                    />
                </View>
            );
        }
        else{
            return null;
        }
    }
}

export default inject('ui', 'userSettings')(observer(CustomTabBar));
