/**
 * @name StackNavTitle
 * @author Zac Steudel
 *
 * @overview Title info from the stack navigation header bar.
 * The code for the user's current number of coins goes here, as well as the popup
 * for when they press the coin icon.
 *
 */

import * as React from 'react';
import { View } from 'react-native';
import { Text, IconButton, Portal, Dialog, Paragraph, Button } from 'react-native-paper';
import { inject, observer } from 'mobx-react';

const StackNavTitle = class StackNavTitle extends React.Component {
	state = {
		menuVisible: false
	};
	render() {
		let lang = this.props.userSettings.language;
		return (
			<View style={{ flexDirection: 'row', marginRight: 10, justifyContent: 'center', alignItems: 'center' }}>
				<Portal>
					<Dialog visible={this.state.menuVisible} onDismiss={() => this.setState({ menuVisible: false })}>
						<Dialog.Title>{lang === 'en' ? 'Health Coins' : 'स्वास्थ्य सिक्के'}</Dialog.Title>
						<Dialog.Content>
							<Paragraph>
								{lang === 'en'
									? 'These are your currency within the app. You can gather Health Coins by logging your food, workouts, and medicine intake. You can also get Health Coins by finishing videos and playing educational games.'
									: 'ये ऐप के भीतर आपकी मुद्रा हैं। आप अपने भोजन, वर्कआउट और दवा के सेवन से स्वास्थ्य के सिक्के एकत्र कर सकते हैं। आप वीडियो खत्म करके और शैक्षिक खेल खेलकर स्वास्थ्य सिक्के भी प्राप्त कर सकते हैं।'}
							</Paragraph>
						</Dialog.Content>
						<Dialog.Actions>
							<Button onPress={() => this.setState({ menuVisible: false })}>
								{lang === 'en' ? 'Done' : 'किया हुआ'}
							</Button>
						</Dialog.Actions>
					</Dialog>
				</Portal>

				<IconButton icon="coin" color="#000000" onPress={() => this.setState({ menuVisible: true })} />
				<Text style={{ fontSize: 19 }}>{this.props.coinState.coins}</Text>
			</View>
		);
	}
};

export default inject('coinState', 'userSettings')(observer(StackNavTitle));
