/**
 * @name LoggingAlertSwitch
 * @author Andrew Case
 *
 * @overview Renders a switch that allows for on-startup logging alerts to be disabled.
 *
 * @example
 * Used in SettingsPage
 * <LoggingAlertSwitch/>
 */

import React from 'react';
import { inject, observer } from 'mobx-react';
import { Text, View, AsyncStorage, Switch } from 'react-native';

const LoggingAlertSwitch = class LoggingAlertSwitch extends React.Component {

    handleChange = (value) => {
        this.props.userSettings.loggingAlerts === "true" ? this.props.userSettings.loggingAlerts = "false" : this.props.userSettings.loggingAlerts = "true";
        AsyncStorage.setItem("loggingAlerts", this.props.userSettings.loggingAlerts);
    }

    render(){
        return(
            <View style = {{alignItems: 'center', flexDirection: "row", justifyContent: "center"}}>
                {this.props.userSettings.language === "en" ? <Text>Logging Alerts</Text> : <Text>लॉगिंग अलर्ट्स</Text>}
                <Switch 
                    value={this.props.userSettings.loggingAlerts === "true"}
                    onValueChange={this.handleChange}
                />
            </View>
        )
    }

    async componentDidMount() {
        this.props.userSettings.loggingAlerts = await AsyncStorage.getItem("loggingAlerts");
    }
}

export default inject('ui', 'userSettings')(observer(LoggingAlertSwitch));