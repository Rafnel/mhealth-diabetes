/**
 * @name FoodLoggingDialogue
 * @author Mario Arturo Lopez Martinez
 * @overview Food view that appears in accordian on Logging Page.
 * @example Used in LoggingPage <FoodLoggingSurface />
 */

import React from 'react';
import FoodLoggingDialogue from './FoodLoggingDialogue';
import * as RootNavigation from '../state/RootNavigation';
import { Surface, Button, Subheading, Divider } from 'react-native-paper';
import { observer, inject } from 'mobx-react';
import { Text } from 'react-native';

const FoodLoggingSurface = class FoodLoggingSurface extends React.Component {
	lang;
	openDialog = () => {
		this.props.ui.foodDialogueOpen = true;
	};

	render() {
		this.lang = this.props.userSettings.language;
		return (
			<Surface style={{ alignItems: 'center', elevation: 3, padding: 10, marginTop: 10 }}>
				<Subheading>{this.lang === 'en' ? 'Food Logging' : 'फूड लॉगिंग'}</Subheading>

				<Button uppercase={false} onPress={this.openDialog} mode="contained">
					{this.lang === 'en' ? 'Add New Food' : 'नया भोजन जोड़ें'}
				</Button>
				<FoodLoggingDialogue />
				<Text />
				<Divider style={{ width: 500 }} />
				<Button uppercase={false} onPress={() => RootNavigation.navigate(this.lang === 'en' ? 'Food History' : 'खाद्य इतिहास')}>
					{this.lang === 'en' ? 'View Food Logging History' : 'खाद्य लॉगिंग इतिहास देखें'}
				</Button>
			</Surface>
		);
	}
};

export default inject('ui', 'userSettings')(observer(FoodLoggingSurface));
