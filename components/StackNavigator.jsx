/**
 * @name StackNavigator
 * @author Zac Steudel
 *
 * @overview Creates the stack navigation scheme for the application. Each screen (view)
 * in the app should have an entry here so that navigation between views is possible.
 *
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { navigationRef } from '../state/RootNavigation';
import HomePage from '../views/HomePage';
import Video1Page from '../views/Video1Page';
import Video2Page from '../views/Video2Page';
import Video3Page from '../views/Video3Page';
import Video4Page from '../views/Video4Page';
import SnakeApp from '../views/Video1InteractionPage';
import PlateApp from '../views/Video2InteractionPage';
import InteractionsPage from '../views/InteractionsPage';
import LoggingPage from '../views/LoggingPage';
import { inject, observer } from 'mobx-react';
import StackNavTitle from './StackNavTitle';
import SettingsPage from '../views/SettingsPage';
import WorkoutHistory from '../views/WorkoutHistory';
import MedicineHistory from '../views/MedicineHistory';
import FoodHistory from '../views/FoodHistory';

const Stack = createStackNavigator();

const StackNavigator = class StackNavigator extends React.Component {
	setCurrentPage = (state) => {
		this.props.ui.page = state.routes[state.index].name;
	};
	render() {
		let lang = this.props.userSettings.language;
		return (
			<NavigationContainer onStateChange={this.setCurrentPage} ref={navigationRef}>
				<Stack.Navigator initialRouteName={lang === 'en' ? 'Home' : 'घर'}>
					<Stack.Screen
						name={lang === 'en' ? 'Home' : 'घर'}
						component={HomePage}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Video 1' : 'वीडियो 1'}
						component={Video1Page}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Video 2' : 'वीडियो 2'}
						component={Video2Page}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Video 3' : 'वीडियो 3'}
						component={Video3Page}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Video 4' : 'वीडियो 4'}
						component={Video4Page}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Game 1' : 'खेल 1'}
						component={SnakeApp}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name = {lang === "en" ? "Game 2" : "खेल 2"}
						component = {PlateApp}
						options = {{headerRight: props => <StackNavTitle/>}}/>
					<Stack.Screen
						name={lang === 'en' ? 'Interactions' : 'सहभागिता'}
						component={InteractionsPage}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Settings' : 'समायोजन'}
						component={SettingsPage}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Logging' : 'लॉगिंग'}
						component={LoggingPage}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Workout History' : 'कसरत का इतिहास'}
						component={WorkoutHistory}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Medicine History' : 'दवा का इतिहास'}
						component={MedicineHistory}
						options={{ headerRight: (props) => <StackNavTitle /> }}
					/>
					<Stack.Screen
						name={lang === 'en' ? 'Food History' : 'खाद्य इतिहास'}
						component={FoodHistory}
						options={{ headerRight: () => <StackNavTitle /> }}
					/>
				</Stack.Navigator>
			</NavigationContainer>
		);
	}
};

export default inject('ui', 'coinState', 'userSettings')(observer(StackNavigator));
