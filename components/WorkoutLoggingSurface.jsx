import React from 'react';
import { Text } from 'react-native';
import { Surface, Button, Subheading, Chip, Divider } from 'react-native-paper';
import { observer, inject } from 'mobx-react';
import WorkoutLoggingDialog from './WorkoutLoggingDialog';
import WorkoutsDatabase from '../utils/workoutDB';
import * as RootNavigation from '../state/RootNavigation';

const WorkoutLoggingSurface = class WorkoutLoggingSurface extends React.Component {
	lang;
	openDialog = () => {
		this.props.ui.workoutDialogOpen = true;
	};

	componentDidMount() {
		let workoutDBService = new WorkoutsDatabase();
		workoutDBService.initDB();
		workoutDBService.getWorkoutsFromToday(this.props.ui);
		workoutDBService.getWorkoutsThisWeek(this.props.ui);
	}

	returnTotalActiveMinutes = () => {
		let activeTime = 0;
		console.log(this.props.ui.workoutsThisWeek.length);
		for (let i = 0; i < this.props.ui.workoutsThisWeek.length; i++) {
			activeTime += this.props.ui.workoutsThisWeek[i].length;
		}
		return <Text style={{ fontWeight: 'bold' }}>{activeTime}</Text>;
	};

	returnWorkoutsDoneToday = () => {
		let workouts = [];

		if (this.props.ui.workoutsToday === null) {
			//no workouts today...
			return <Text>{this.lang === 'en' ? 'None' : 'कोई नहीं'}</Text>;
		}

		for (let i = 0; i < this.props.ui.workoutsToday._array.length; i++) {
			let currentWorkout = this.props.ui.workoutsToday._array[i];
			let textToShow;
			if (this.lang === 'en') {
				textToShow = currentWorkout.type + ', Time Spent: ' + currentWorkout.length + ' minutes';
			} else {
				let hindiWorkout;
				switch (currentWorkout.type) {
					case 'Yoga':
						hindiWorkout = 'योग';
						break;
					case 'Walking':
						hindiWorkout = 'चलना';
						break;
					case 'Running':
						hindiWorkout = 'चल रहा है';
						break;
					case 'Swimming':
						hindiWorkout = 'तैराकी';
						break;
					case 'Sport':
						hindiWorkout = 'खेल';
						break;
					case 'Other':
						hindiWorkout = 'अन्य';
						break;
				}
				textToShow = hindiWorkout + ', समय बिताया: ' + currentWorkout.length + ' मिनट';
			}
			let icon = 'heart-pulse';

			if (currentWorkout.type === 'Walking') {
				icon = 'walk';
			} else if (currentWorkout.type === 'Running') {
				icon = 'run-fast';
			} else if (currentWorkout.type === 'Swimming') {
				icon = 'swim';
			}

			workouts.push(
				<Chip key={i} icon={icon}>
					{textToShow}
				</Chip>
			);
			//push some spacing
			workouts.push(<Text key={i + 100} />);
		}

		return workouts;
	};

	render() {
		this.lang = this.props.userSettings.language;
		return (
			<Surface style={{ alignItems: 'center', elevation: 3, padding: 10, marginTop: 10 }}>
				<Subheading>{this.lang === 'en' ? 'Workout Logging' : 'वर्कआउट लॉगिंग'}</Subheading>
				<Text>
					{this.lang === 'en'
						? 'Get rewards for working out!'
						: 'बाहर काम करने के लिए पुरस्कार प्राप्त करें!'}
				</Text>
				<Text />
				<Button uppercase={false} onPress={this.openDialog} mode="contained">
					{this.lang === 'en' ? 'Enter Workout' : 'वर्कआउट दर्ज करें'}
				</Button>
				<WorkoutLoggingDialog />
				<Text />
				<Text>{this.lang === 'en' ? 'Workouts done today:' : 'आज किए गए वर्कआउट:'}</Text>
				{this.returnWorkoutsDoneToday()}
				<Divider style={{ width: 500 }} />
				<Text>
					{this.lang === 'en' ? 'Total Active Minutes This Week:' : 'इस सप्ताह कुल सक्रिय मिनट:'}{' '}
					{this.returnTotalActiveMinutes()}
				</Text>
				<Text />
				<Divider style={{ width: 500 }} />
				<Button
					uppercase={false}
					onPress={() => RootNavigation.navigate(this.lang === 'en' ? 'Workout History' : 'कसरत का इतिहास')}
				>
					{this.lang === 'en' ? 'View Workout Logging History' : 'वर्कआउट लॉगिंग इतिहास देखें'}
				</Button>
			</Surface>
		);
	}
};

export default inject('ui', 'userSettings')(observer(WorkoutLoggingSurface));
