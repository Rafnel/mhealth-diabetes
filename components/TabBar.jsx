import React from 'react';
import { inject, observer } from 'mobx-react';
import { View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import HomePage from '../views/HomePage';
import Video1Page from '../views/Video1Page';
import Game11 from '../views/Video1InteractionPage';
import Video2Page from '../views/Video2Page';
import Video3Page from '../views/Video3Page';
import Video4Page from '../views/Video4Page';
import TakePhotoPage from '../views/TakePhotoPage';

//THIS IS NOT IN USE, DEPRECATED, SEE CUSTOMTABBAR FOR WHAT WE ARE USING AS THE TAB BAR

// create a component that can react to change
const TabBar = class TabBar extends React.Component {
	render() {
		//Take a look at the Home: route. As you can see, there is a tabBarVisible prop that is passed to it.
		//When you click that button on home page, it negates that mobx bottomNavigatorVisible state variable.
		//All we had to do to make the nav bar reactive to state changes is wrap it in our own custom
		//component, which we now have done.
		const Routes = {
			Home: {
				screen: HomePage,
				navigationOptions: {
					tabBarLabel: 'Home',
					tabBarVisible: this.props.ui.bottomNavigatorVisible,
					tabBarIcon: ({ tintColor }) => (
						<View>
							<Icon style={[{ color: tintColor }]} size={25} name={'ios-home'} />
						</View>
					)
				}
			},
			Game0: {
				screen: Game11,
				navigationOptions: {
					tabBarLabel: 'GameMaube',
					tabBarIcon: ({ tintColor }) => (
						<View>
							<Icon style={[{ color: tintColor }]} size={25} name={'ios-home'} />
						</View>
					)
				}
			},
			Game1: {
				screen: Video1Page,
				navigationOptions: {
					tabBarLabel: 'Game1',
					tabBarIcon: ({ tintColor }) => (
						<View>
							<Icon style={[{ color: tintColor }]} size={25} name={'md-lock'} />
						</View>
					)
				}
			},
			Game2: {
				screen: Video2Page,
				navigationOptions: {
					tabBarLabel: 'Game2',
					tabBarIcon: ({ tintColor }) => (
						<View>
							<Icon style={[{ color: tintColor }]} size={25} name={'md-lock'} />
						</View>
					)
				}
			},
			Game3: {
				screen: Video3Page,
				navigationOptions: {
					tabBarLabel: 'Game3',
					tabBarIcon: ({ tintColor }) => (
						<View>
							<Icon style={[{ color: tintColor }]} size={25} name={'md-lock'} />
						</View>
					)
				}
			},
			Game4: {
				screen: Video4Page,
				navigationOptions: {
					tabBarLabel: 'Game4',
					tabBarIcon: ({ tintColor }) => (
						<View>
							<Icon style={[{ color: tintColor }]} size={25} name={'md-lock'} />
						</View>
					)
				}
			}
		};

		const TabConfig = {
			initialRouteName: 'Home',
			activeColor: '#f0edf6',
			inactiveColor: '#226557',
			barStyle: { backgroundColor: '#3BAD87' },
			defaultNavigationOptions: ({ navigation }) => ({
				tabBarOnPress: ({ navigation, defaultHandler }) => {
					if (navigation.state.routeName === 'Game2') {
						return null;
					}
					defaultHandler();
				}
			})
		};

		const TabNav = createMaterialBottomTabNavigator(Routes, TabConfig);

		//only use this if we want a top tab bar.
		// const StackNav = createStackNavigator({
		// 	Tabs: {
		// 		screen: TabNav,
		// 		navigationOptions: { title: 'mHealth Diabetes',
		// 			headerStyle: {backgroundColor: '#3BAD87'},
		// 			headerTintColor: '#f0edf6',
		// 			headerTitleStyle: {
		// 				fontWeight: 'bold'
		// 			},
		// 			headerLeft: () => (
		// 				<View style={{padding: 10, alignSelf: 'flex-start'}}>
		// 					<Image style={{width: 50, height: 50}} source={require('../assets/icon.png')}/>
		// 				</View>
		// 			),
		// 			headerRight: () => (
		// 				<View style={{padding: 20, alignSelf: 'flex-end'}}>
		// 					<Icon
		// 						style={[{color: '#f0edf6'}]}
		// 						size={25}
		// 						name={Platform.OS === 'ios' ? 'ios-settings':'md-settings'}
		// 					/>
		// 				</View>
		// 			)
		// 		}
		// 	}
		// })

		const App = createAppContainer(TabNav);
		return <App />;
	}
};

//inject state into the tab bar
export default inject('ui')(observer(TabBar));
