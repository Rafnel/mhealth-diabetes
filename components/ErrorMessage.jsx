/**
 * @name ErrorMessage
 * @author Andrew Case & Zac Steudel
 *
 * @overview Red popup notification that appears to warn the user of an action.
 *
 */

import * as React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { Snackbar } from 'react-native-paper';
import { inject, observer } from 'mobx-react';

const ErrorMessage = class ErrorMessage extends React.Component {
	render() {
		return (
			<TouchableWithoutFeedback onPress={() => this.props.notifications.setErrorNotification('')}>
				<Snackbar
					style={{ bottom: 49, backgroundColor: '#CF2424' }}
					duration={3000}
					visible={this.props.notifications.errorNotification.length !== 0}
					onDismiss={() => this.props.notifications.setErrorNotification('')}
				>
					{this.props.notifications.errorNotification}
				</Snackbar>
			</TouchableWithoutFeedback>
		);
	}
};

export default inject('notifications')(observer(ErrorMessage));
