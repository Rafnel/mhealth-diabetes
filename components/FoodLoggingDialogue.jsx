/**
 * @name FoodLoggingDialogue
 * @author Mario Arturo Lopez Martinez
 * @overview The dialog for entering food information.
 * @example Used in FoodLoggingSurface <FoodLoggingDialog />
 */

import React from 'react';
import FoodDatabase from '../utils/foodDB';
import * as ImagePicker from 'expo-image-picker';
import * as MediaLibrary from 'expo-media-library';
import { Portal, Dialog, Button, TextInput, Switch } from 'react-native-paper';
import { Text, Image, StyleSheet, ScrollView } from 'react-native';
import { updateHealthCoins } from '../utils/coinStorage';
import { observer, inject } from 'mobx-react';
import { Camera } from 'expo-camera';

const styles = StyleSheet.create({
	scrollView: {
		height: 100,
	},
});

const FoodLoggingDialogue = class FoodLoggingDialogue extends React.Component {
	lang;
	constructor(props) {
		super(props);

		this.state = {
			hasPermission: null,
			type: Camera.Constants.Type.back,
			foodDescription: '',
			foodIsHealthy: false,
			photo: null,
		};
	}

	async componentDidMount() {
		this.lang = this.props.userSettings.language;
		const { status } = await Camera.requestPermissionsAsync();
		this.setState({
			hasPermission: status === 'granted',
			photo: this.props.ui.foodLoggingPictureUri,
			foodIsHealthy: false,
		});
	}

	_onToggleSwitch = () => this.setState({ foodIsHealthy: !this.state.foodIsHealthy });

	_closeDialog = () => {
		this.props.ui.foodDialogueOpen = false;
	};

	_onSubmit = () => {
		// Add food entry to the local DB
		let foodDBService = new FoodDatabase();
		foodDBService.insertNewFood(
			this.state.foodDescription,
			this.state.photo,
			this.state.foodIsHealthy,
			this.props.ui
		);
		this.props.ui.foodDialogueOpen = false;

		this.props.notifications.successNotification = (this.lang === 'en' ? 'Meal successfully entered! You gained 1 Health Coin' : 'भोजन सफलतापूर्वक प्रवेश किया! आपने 1 स्वास्थ्य सिक्का प्राप्त किया');
		this.props.coinState.coins++;

		updateHealthCoins(this.props.coinState.coins);

		this.setState({ foodDescription: '' });
		this.setState({ photo: null });
		this.setState({ foodIsHealthy: false });
	};

	_openImagePickerAsync = async () => {
		let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();

		if (permissionResult.granted === false) {
			alert('Permission to access camera roll is required!');
			return;
		}

		let pickerResult = await ImagePicker.launchImageLibraryAsync();

		if (pickerResult.cancelled === true) {
			return;
		}

		this.setState({ photo: pickerResult.uri });
	};

	_openCamera = async () => {
		let permissionResult = await ImagePicker.requestCameraPermissionsAsync();
		if (permissionResult.granted === false) {
			alert('Permission to access camera is required!');
			return;
		}

		let pickerResult = await ImagePicker.launchCameraAsync();
		if (pickerResult.cancelled === true) {
			return;
		}

		this.setState({ photo: pickerResult.uri });

		const asset = await MediaLibrary.createAssetAsync(pickerResult.uri);
		const album = await MediaLibrary.getAlbumAsync('mHealth');

		if (album == null) {
			MediaLibrary.createAlbumAsync('mHealth', asset, false);
		} else {
			MediaLibrary.addAssetsToAlbumAsync([asset], album, false);
		}
	};

	render() {
		const { photo } = this.state;

		return (
			<Portal>
				<Dialog styles={styles.scrollView} visible={this.props.ui.foodDialogueOpen}>
					<Dialog.Title>{this.lang === 'en' ? 'Log Food' : 'लॉग खाना'}</Dialog.Title>
					<Dialog.Content>
						<ScrollView>
							<Button onPress={this._openCamera}>{this.lang === 'en' ? 'Take Photo' : 'फोटो लो'}</Button>
							{this.state.photo && (
								<Image
									style={{ width: 200, height: 300, marginBottom: 10, resizeMode: 'contain' }}
									source={{ isStatic: true, uri: this.state.photo }}
								/>
							)}
							<Button mode="contained" onPress={this._openImagePickerAsync}>
								{!photo && this.lang === 'en' ? 'Choose Photo' : 'फोटो चुनो'}
								{photo && this.lang === 'en' ? 'Change Photo' : 'तस्वीर बदलिये'}
							</Button>
							<TextInput
								style={{ marginTop: 25 }}
								label={this.lang === 'en' ? 'Describe your meal!' : 'अपने भोजन का वर्णन करें!'}
								mode="outlined"
								value={this.state.foodDescription}
								onChangeText={(text) => {
									this.setState({ foodDescription: text });
								}}
							/>
							<Text style={{ marginTop: 25 }}>{this.lang === 'en' ? 'Is your food healthy?' : 'क्या आपका भोजन स्वस्थ है?'}</Text>
							<Switch value={this.state.foodIsHealthy} onValueChange={this._onToggleSwitch} />
							<Text>
								{this.state.foodIsHealthy && this.lang === 'en' ? 'Yes' : 'हाँ'}
								{!this.state.foodIsHealthy && this.lang === 'en' ? 'No' : 'नहीं'}
							</Text>
						</ScrollView>
					</Dialog.Content>
					<Dialog.Actions>
						<Button mode="outlined" onPress={this._closeDialog}>
							{this.lang === 'en' ? 'Cancel' : 'रद्द करना'}
						</Button>
						<Text>&nbsp;</Text>
						<Button mode="contained" onPress={this._onSubmit}>
							{this.lang === 'en' ? 'Save' : 'सहेजें'}
						</Button>
					</Dialog.Actions>
				</Dialog>
			</Portal>
		);
	}
};

export default inject('ui', 'notifications', 'coinState', 'userSettings')(observer(FoodLoggingDialogue));
