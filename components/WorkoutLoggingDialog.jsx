import React from 'react';
import { Text, Picker } from 'react-native';
import { observer, inject } from 'mobx-react';
import { Portal, Dialog, Button, TextInput } from 'react-native-paper';
import WorkoutsDatabase from '../utils/workoutDB';
import { updateExercise } from '../utils/avatarStorage';
import { updateHealthCoins } from '../utils/coinStorage';

const WorkoutLoggingDialog = class WorkoutLoggingDialog extends React.Component {
	lang;
	constructor(props) {
		super(props);

		this.state = {
			workoutType: '',
			workoutLength: '',
			workoutLengthError: false,
		};
	}

	closeDialog = () => {
		this.props.ui.workoutDialogOpen = false;
	};

	checkLengthValid = (text) => {
		if (isNaN(text) || text === '') {
			this.setState({ workoutLengthError: true });
		} else {
			this.setState({ workoutLengthError: false });
		}
	};

	onSubmit = () => {
		//add this workout to the local DB of workouts. Also need to
		//display this on the list of workouts for today on the workout logging surface
		let workoutDBService = new WorkoutsDatabase();
		workoutDBService.insertNewWorkout(this.state.workoutType, this.state.workoutLength, this.props.ui);
		this.props.ui.workoutDialogOpen = false;

		//update the avatar's exercise state!
		updateExercise(this.state.workoutLength);
		let coinsGained = this.state.workoutLength / 10;
		coinsGained = Math.trunc(coinsGained);
		this.props.notifications.successNotification =
			this.lang === 'en'
				? 'Workout successfully entered! Your pet gained ' +
				  this.state.workoutLength * 10 +
				  ' exercise points and you gained ' +
				  coinsGained +
				  ' Health Coins'
				: 'वर्कआउट सफलतापूर्वक प्रवेश किया! आपके पालतू ने ' +
				  this.state.workoutLength * 10 +
				  ' व्यायाम अंक प्राप्त किए और आपने ' +
				  coinsGained +
				  ' स्वास्थ्य सिक्के प्राप्त किए';

		this.props.coinState.coins += coinsGained;
		updateHealthCoins(this.props.coinState.coins);

		workoutDBService.getWorkoutsThisWeek(this.props.ui);
		workoutDBService.getWorkoutsFromToday(this.props.ui);
	};

	render() {
		this.lang = this.props.userSettings.language;
		return (
			<Portal>
				<Dialog visible={this.props.ui.workoutDialogOpen} onDismiss={this.closeDialog}>
					<Dialog.Title>{this.lang === 'en' ? 'Log a Workout' : 'एक लॉगआउट करें'}</Dialog.Title>
					<Dialog.Content>
						<Text>
							{this.lang === 'en'
								? 'First, select the type of workout you did.'
								: 'सबसे पहले, आपके द्वारा किए गए वर्कआउट के प्रकार का चयन करें।'}
						</Text>
						<Picker
							mode="dropdown"
							selectedValue={this.state.workoutType}
							style={{ height: 50, width: '100%' }}
							onValueChange={(itemValue, itemIndex) => this.setState({ workoutType: itemValue })}
						>
							<Picker.Item
								label={this.lang === 'en' ? 'Select a workout...' : 'एक कसरत का चयन करें ...'}
								value=""
							/>
							<Picker.Item label={this.lang === 'en' ? 'Yoga' : 'योग'} value="Yoga" />
							<Picker.Item label={this.lang === 'en' ? 'Walking' : 'चलना'} value="Walking" />
							<Picker.Item label={this.lang === 'en' ? 'Running' : 'चल रहा है'} value="Running" />
							<Picker.Item label={this.lang === 'en' ? 'Swimming' : 'तैराकी'} value="Swimming" />
							<Picker.Item label={this.lang === 'en' ? 'Sport' : 'खेल'} value="Sport" />
							<Picker.Item label={this.lang === 'en' ? 'Other' : 'अन्य'} value="Other" />
						</Picker>
						<Text>
							{this.lang === 'en'
								? 'Next, enter how many minutes your workout was.'
								: 'इसके बाद, दर्ज करें कि आपका वर्कआउट कितने मिनट का था।'}
						</Text>
						<TextInput
							label={this.lang === 'en' ? 'Workout Length (Minutes)' : 'कसरत की लंबाई (मिनट)'}
							mode="outlined"
							keyboardType="number-pad"
							error={this.state.workoutLengthError}
							value={this.state.workoutLength}
							onChangeText={(text) => {
								this.setState({ workoutLength: text });
								this.checkLengthValid(text);
							}}
						/>
					</Dialog.Content>
					<Dialog.Actions>
						<Button mode="outlined" onPress={this.closeDialog}>
							{this.lang === 'en' ? 'Cancel' : 'रद्द करना'}
						</Button>
						<Text>&nbsp;</Text>
						<Button
							disabled={
								this.state.workoutLengthError ||
								this.state.workoutLength === '' ||
								this.state.workoutType === ''
							}
							mode="contained"
							onPress={this.onSubmit}
						>
							{this.lang === 'en' ? 'Save' : 'सहेजें'}
						</Button>
					</Dialog.Actions>
				</Dialog>
			</Portal>
		);
	}
};

export default inject('ui', 'notifications', 'coinState', 'userSettings')(observer(WorkoutLoggingDialog));
