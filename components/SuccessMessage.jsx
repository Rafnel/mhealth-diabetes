/**
 * @name SuccessMessage
 * @author Andrew Case & Zac Steudel
 *
 * @overview Green popup notification that appears to show the user of an action.
 *
 */

import * as React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { Snackbar } from 'react-native-paper';
import { inject, observer } from 'mobx-react';

const SuccessMessage = class SuccessMessage extends React.Component {
	render() {
		return (
			<TouchableWithoutFeedback onPress={() => this.props.notifications.setSuccessNotification('')}>
				<Snackbar
					style={{ bottom: 49, backgroundColor: '#0FBF52' }}
					duration={5500}
					visible={this.props.notifications.successNotification.length !== 0}
					onDismiss={() => this.props.notifications.setSuccessNotification('')}
				>
					{this.props.notifications.successNotification}
				</Snackbar>
			</TouchableWithoutFeedback>
		);
	}
};

export default inject('notifications')(observer(SuccessMessage));
