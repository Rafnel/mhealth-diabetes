/**
 * @name SnakeConstants.jsx
 * @author Michael Ibanez
 *
 * @overview Contains components that the snake game uses
 *
 * @example
 * Used in Video1InteractionPage
 * <Food> </Food>
 * <Head> </Head>
 * <Tail> </Tail>
 * <GameLoop> </GameLoop>
 */

import React, { Component } from 'react';
import { Dimensions, StyleSheet, View, Image } from 'react-native';
import Images from '../assets/snakeSprites/images';
import Constants from './Constants';

class Food extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const x = this.props.position[0];
		const y = this.props.position[1];
		let image = Images['fruit' + this.props.choice];

		return (
			<Image
				source={image}
				style={[
					styles.FoodStyle,
					{
						width: this.props.size,
						height: this.props.size,
						left: x * this.props.size,
						top: y * this.props.size
					}
				]}
			/>
		);
	}
}

class Head extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const x = this.props.position[0];
		const y = this.props.position[1];
		let image = Images['mon' + this.props.pose];
		return (
			<Image
				source={image}
				style={[
					styles.HeadStyle,
					{
						width: this.props.size,
						height: this.props.size,
						left: x * this.props.size,
						top: y * this.props.size
					}
				]}
			/>
		);
	}
}

const randomBetween = (min, max) => {
	return Math.floor(Math.random() * (max - min + 1) + min);
};

const GameLoop = (entities, { touches, dispatch, events }) => {
	let head = entities.head;
	let food = entities.food;
	let tail = entities.tail;

	if (events.length) {
		for (let i = 0; i < events.length; i++) {
			if (events[i].type === 'move-down' && head.yspeed != -1) {
				head.yspeed = 1;
				head.xspeed = 0;
				head.pose = 1;
			} else if (events[i].type === 'move-up' && head.yspeed != 1) {
				head.yspeed = -1;
				head.xspeed = 0;
				head.pose = 3;
			} else if (events[i].type === 'move-left' && head.xspeed != 1) {
				head.yspeed = 0;
				head.xspeed = -1;
				head.pose = 2;
			} else if (events[i].type === 'move-right' && head.xspeed != -1) {
				head.yspeed = 0;
				head.xspeed = 1;
				head.pose = 4;
			}
		}
	}

	head.nextMove -= 1;
	if (head.nextMove === 0) {
		head.nextMove = head.updateFrequency;
		if (
			head.position[0] + head.xspeed < 0 ||
			head.position[0] + head.xspeed >= Constants.GRID_SIZE ||
			head.position[1] + head.yspeed < 0 ||
			head.position[1] + head.yspeed >= Constants.GRID_SIZE
		) {
			// snake hits the wall
			dispatch({ type: 'game-over' });
		} else {
			// move the tail
			for (let i = tail.elements.length; i > 0; i--) {
				if (i == 1) {
					tail.elements[0] = [head.position[0], head.position[1], tail.elements[0][2]];
				} else {
					tail.elements[i - 1] = [tail.elements[i - 2][0], tail.elements[i - 2][1], tail.elements[i - 1][2]];
				}
			}

			// snake moves
			head.position[0] += head.xspeed;
			head.position[1] += head.yspeed;

			// check if it hits the tail
			for (let i = 0; i < tail.elements.length; i++) {
				if (tail.elements[i][0] === head.position[0] && tail.elements[i][1] === head.position[1]) {
					dispatch({ type: 'game-over' });
				}
			}

			if (head.position[0] === food.position[0] && head.position[1] === food.position[1]) {
				// eating Food
				tail.elements = [[food.position[0], food.position[1], food.choice]].concat(tail.elements);
				food.position[0] = randomBetween(0, Constants.GRID_SIZE - 1);
				food.position[1] = randomBetween(0, Constants.GRID_SIZE - 1);
				food.choice = randomBetween(1, 7);
				dispatch({ type: 'scored' });
			}
		}
	}
	return entities;
};

class Tail extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let tailList = this.props.elements.map((el, idx) => {
			let image = Images['fruit' + el[2]];
			return (
				<Image
					source={image}
					key={idx}
					style={{
						width: this.props.size,
						height: this.props.size,
						position: 'absolute',
						left: el[0] * this.props.size,
						top: el[1] * this.props.size,
						backgroundColor: 'white'
					}}
				/>
			);
		});

		return (
			<View
				style={{ width: Constants.GRID_SIZE * this.props.size, height: Constants.GRID_SIZE * this.props.size }}
			>
				{tailList}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	finger: {
		backgroundColor: '#888888',
		position: 'absolute'
	},
	FoodStyle: {
		backgroundColor: 'white',
		position: 'absolute'
	},
	HeadStyle: {
		backgroundColor: 'white',
		position: 'absolute'
	}
});

export { Head, Tail, Food, GameLoop };
