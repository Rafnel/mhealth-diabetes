/**
 * @name MedicineLoggingDialog
 * @author Andrew Case
 *
 * @overview The dialog for entering medicine information.
 *
 * @example
 * Used in MedicineLoggingSurface
 * <MedicineLoggingDialog/>
 */

import React from 'react';
import { Text, Picker } from 'react-native';
import { observer, inject } from 'mobx-react';
import { Portal, Dialog, Button, TextInput } from 'react-native-paper';
import MedicineDatabase from '../utils/medicineDB';
import { updateHealthCoins } from '../utils/coinStorage';

const MedicineLoggingDialog = class MedicineLoggingDialog extends React.Component {
	lang;
	constructor(props) {
		super(props);

		this.state = {
			dosage: '',
			medicationName: '',
			medicationNameError: false,
			previousMedicationSelected: '',
		};
	}

	componentDidMount() {
		let medicineDBService = new MedicineDatabase();
		medicineDBService.initDB();
		medicineDBService.previouslyEnteredMedication(this.props.ui);
	}

	closeDialog = () => {
		this.props.ui.medicineDialogOpen = false;
	};

	checkLengthValid = (text) => {
		if (text === '') {
			this.setState({ medicationNameError: true });
		} else {
			this.setState({ medicationNameError: false });
		}
	};

	previouslyEnteredMedication() {
		let results = [];
		if (this.props.ui.previouslyEnteredMeds !== null) {
			results.push(
				<Picker.Item
					key={1}
					label={this.lang === 'en' ? 'Select a previous medication...' : 'पिछली दवा का चयन करें...'}
					value={this.lang === 'en' ? 'Select a previous medication...' : 'पिछली दवा का चयन करें...'}
				/>
			);
			for (let i = 0; i < this.props.ui.previouslyEnteredMeds._array.length; i++) {
				results.push(
					<Picker.Item
						key={i}
						label={this.props.ui.previouslyEnteredMeds._array[i].name}
						value={this.props.ui.previouslyEnteredMeds._array[i].name}
					/>
				);
			}
		}
		results.push(
			<Picker.Item
				key="key"
				label={this.lang === 'en' ? 'New Medication...' : 'नई दवा...'}
				value={this.lang === 'en' ? 'New Medication...' : 'नई दवा...'}
			/>
		);
		return results;
	}

	onSubmit = () => {
		// Add this medication to the local DB
		// Also display this on the list of medication for today on the medicine logging surface
		let medicineDBService = new MedicineDatabase();
		let name = '';
		if (
			this.state.previousMedicationSelected === 'New Medication...' ||
			this.state.previousMedicationSelected === 'नई दवा...'
		) {
			name = this.state.medicationName;
		} else {
			name = this.state.previousMedicationSelected;
		}
		medicineDBService.insertNewMedicine(this.state.dosage, name, this.props.ui);
		this.props.ui.medicineDialogOpen = false;

		this.props.notifications.successNotification =
			this.lang === 'en'
				? 'Medicine successfully entered! You gained 1 Health Coin!'
				: 'दवा सफलतापूर्वक दर्ज की गई! आपने 1 स्वास्थ्य सिक्का प्राप्त किया!';

		this.props.coinState.coins++;
		updateHealthCoins(this.props.coinState.coins);
		this.setState({ dosage: '' });
		this.setState({ medicationName: '' });
		this.setState({ previousMedicationSelected: '' });
		this.setState({ medicationNameError: false });
		medicineDBService.previouslyEnteredMedication(this.props.ui);
	};

	render() {
		this.lang = this.props.userSettings.language;
		return (
			<Portal>
				<Dialog visible={this.props.ui.medicineDialogOpen} onDismiss={this.closeDialog}>
					<Dialog.Title>{this.lang === 'en' ? 'Log Medicine' : 'लॉग चिकित्सा'}</Dialog.Title>
					<Dialog.Content>
						<Text>
							{this.lang === 'en'
								? 'First, select the dosage of the medicine you took.'
								: 'सबसे पहले, आपके द्वारा ली गई दवा की खुराक का चयन करें।'}
						</Text>
						<Picker
							mode="dropdown"
							selectedValue={this.state.dosage}
							style={{ height: 50, width: '100%' }}
							onValueChange={(itemValue, itemIndex) => this.setState({ dosage: itemValue })}
						>
							<Picker.Item
								key={1}
								label={this.lang === 'en' ? 'Select a dosage...' : 'एक खुराक का चयन करें...'}
								value=""
							/>
							<Picker.Item
								key={2}
								label={this.lang === 'en' ? 'Once daily' : 'एक बार रोज़'}
								value="OnceDaily"
							/>
							<Picker.Item
								key={3}
								label={this.lang === 'en' ? 'Twice daily' : 'दिन में दो बार'}
								value="TwiceDaily"
							/>
							<Picker.Item
								key={4}
								label={this.lang === 'en' ? 'Three times daily' : 'तीन बार दैनिक'}
								value="ThreeDaily"
							/>
							<Picker.Item
								key={5}
								label={this.lang === 'en' ? 'Once weekly' : 'एक बार साप्ताहिक'}
								value="OnceWeekly"
							/>
							<Picker.Item
								key={6}
								label={this.lang === 'en' ? 'Twice weekly' : 'सप्ताह में दो बार'}
								value="TwiceWeekly"
							/>
							<Picker.Item key={7} label={this.lang === 'en' ? 'Other' : 'अन्य'} value="Other" />
						</Picker>
						<Text>
							{this.lang === 'en'
								? 'Next, enter the name of the medicine.'
								: 'इसके बाद, दवा का नाम दर्ज करें।'}
						</Text>
						{this.props.ui.previouslyEnteredMeds !== null ? (
							<Picker
								mode="dropdown"
								selectedValue={this.state.previousMedicationSelected}
								style={{ height: 50, width: '100%' }}
								onValueChange={(itemValue, itemIndex) => {
									itemValue === 'Select a previous medication...' ||
									itemValue === 'पिछली दवा का चयन करें...'
										? this.setState({ previousMedicationSelected: '', medicationName: '' })
										: this.setState({ previousMedicationSelected: itemValue });
								}}
							>
								{this.previouslyEnteredMedication()}
							</Picker>
						) : null}
						{this.state.previousMedicationSelected === 'New Medication...' ||
						this.state.previousMedicationSelected === 'नई दवा...' ? (
							<TextInput
								label={this.lang === 'en' ? 'Medicine Name' : 'दवा का नाम'}
								mode="outlined"
								error={this.state.medicationNameError}
								value={this.state.medicationName}
								onChangeText={(text) => {
									this.setState({ medicationName: text });
									this.checkLengthValid(text);
								}}
							/>
						) : null}
					</Dialog.Content>
					<Dialog.Actions>
						<Button mode="outlined" onPress={this.closeDialog}>
							{this.lang === 'en' ? 'Cancel' : 'रद्द करना'}
						</Button>
						<Text>&nbsp;</Text>
						<Button
							disabled={
								this.state.medicationNameError ||
								this.state.dosage === '' ||
								this.state.previousMedicationSelected === '' ||
								(this.state.medicationName === '' &&
									this.state.previousMedicationSelected === 'New Medication...') ||
								(this.state.medicationName === '' &&
									this.state.previousMedicationSelected === 'नई दवा...')
							}
							mode="contained"
							onPress={this.onSubmit}
						>
							{this.lang === 'en' ? 'Save' : 'सहेजें'}
						</Button>
					</Dialog.Actions>
				</Dialog>
			</Portal>
		);
	}
};

export default inject('ui', 'notifications', 'coinState', 'userSettings')(observer(MedicineLoggingDialog));
