import React from 'react';
import { Text, AsyncStorage, Image, View, ImageBackground } from 'react-native';
import { Surface, Chip } from 'react-native-paper';
import { inject, observer } from 'mobx-react';
import { updateAvatarState } from '../../utils/avatarStorage';
import WorkoutsDatabase from '../../utils/workoutDB';

const Avatar = class Avatar extends React.Component {
	lang;
	returnAvatarMessage(){
		let messageString = "";
		if(this.props.avatar.hunger < 5 && this.props.avatar.thirst < 5){
			messageString = this.lang === "en" ? "I'm not very hungry or thirsty." : "मैं बहुत भूखा या प्यासा नहीं हूँ।";
		}
		else if(this.props.avatar.hunger >= 5 && this.props.avatar.hunger < 20 && this.props.avatar.thirst >= 5 && this.props.avatar.thirst < 20){
			messageString = this.lang === "en" ? "I could use a healthy snack and some water!" : "मैं एक स्वस्थ स्नैक और कुछ पानी का उपयोग कर सकता था!";
		}
		else if(this.props.avatar.hunger >= 5 && this.props.avatar.hunger < 20){
			messageString = this.lang === "en" ? "I could use a healthy snack!" : "मैं एक स्वस्थ नाश्ते का उपयोग कर सकता हूं!";
		}
		else if(this.props.avatar.thirst >= 5 && this.props.avatar.thirst < 20){
			messageString = this.lang === "en" ? "I could use some water!" : "मैं कुछ पानी का उपयोग कर सकता हूं!";
		}
		else if(this.props.avatar.hunger >= 20 && this.props.avatar.hunger < 50 && this.props.avatar.thirst >= 20 && this.props.avatar.thirst < 50){
			messageString = this.lang === "en" ? "I am pretty hungry and thirsty. I could use a healthy meal and a big glass of water!" : "मैं बहुत भूखा और प्यासा हूँ। मैं एक स्वस्थ भोजन और एक बड़ा गिलास पानी इस्तेमाल कर सकता था!";
		}
		else if(this.props.avatar.hunger >= 20 && this.props.avatar.hunger < 50){
			messageString = this.lang === "en" ? "I am pretty hungry. I could use a healthy meal!" : "मुझे बहुत भूख लगी है। मैं एक स्वस्थ भोजन का उपयोग कर सकता था!";
		}
		else if(this.props.avatar.thirst >= 20 && this.props.avatar.thirst < 50){
			messageString = this.lang === "en" ? "I am pretty thirsty. I could use a big glass of water!" : "मैं बहुत प्यासी हूँ। मैं एक बड़ा गिलास पानी इस्तेमाल कर सकता था!";
		}
		else if(this.props.avatar.hunger >= 50 && this.props.avatar.thirst >= 50){
			messageString = this.lang === "en" ? "I am very hungry and thirsty. I could use a big healthy meal and a large glass of water!" : "मैं बहुत भूखा और प्यासा हूं। मैं एक बड़ा स्वस्थ भोजन और एक बड़ा गिलास पानी इस्तेमाल कर सकता था!";
		}
		else if(this.props.avatar.hunger >= 50){
			messageString = this.lang === "en" ? "I am very hungry. I could use a big healthy meal!" : "मैं बहुत भूखा हूँ। मैं एक बड़े स्वस्थ भोजन का उपयोग कर सकता था!";
		}
		else if(this.props.avatar.thirst >= 50){
			messageString = this.lang === "en" ? "I am very thirsty. I could use a large glass of water!" : "मुझे बहुत प्यास लगी है। मैं एक बड़े गिलास पानी का उपयोग कर सकता था!";
		}

		return <Text>{messageString}</Text>
	}

	returnAvatarExerciseMessage = () => {
		let messageString = "";
		if(this.props.avatar.exercise >= 700){
			messageString = this.lang === "en" ? "I have been having a great time working out and staying fit!" : "मुझे काम करने और फिट रहने के लिए एक अच्छा समय मिल रहा है!";
		}
		else if(this.props.avatar.exercise >= 500){
			messageString = this.lang === "en" ? "I would like to get a short walk or some other exercise!" : "मैं एक छोटी सैर या कुछ अन्य व्यायाम करना चाहूंगा!";
		}
		else if(this.props.avatar.exercise < 500){
			messageString = this.lang === "en" ? "I would like to do a big workout and get healthy!!" : "मैं एक बड़ी कसरत करना और स्वस्थ होना चाहूंगा !!";
		}

		return <Text>{messageString}</Text>
	}

	returnUserWorkoutMessage = () => {
		let length = this.props.ui.lengthOfWorkingOutToday;
		if(this.props.ui.lengthOfWorkingOutToday > 0){
			return <Text>{this.lang === "en" ? "You have spent " + length + " minutes working out today. Your pet thanks you!" : "आज आपने " + length + " मिनट बिताए हैं। आपका पालतू धन्यवाद!"}</Text>
		}
		else{
			return null;
		}
	}

	returnPetImage = () => {
		//function will determine what image to use based on state of the pet (hunger, thirst, exercise)
		if(this.props.avatar.thirst < 20 && this.props.avatar.hunger < 20 && this.props.avatar.exercise >= 500){
			return <ImageBackground style = {{width: 200, height: 200}} resizeMode='contain' source={require('../../assets/avatarImages/happy_lab.png')}>
				{this.returnAvatarItemsIfHappy()}
			</ImageBackground>
		}
		else if(this.props.avatar.thirst >= 20 || this.props.avatar.hunger >= 20){
			return <ImageBackground style= {{width: 200, height: 200}} resizeMode="contain" source={require('../../assets/avatarImages/hungry_thirsty.jpeg')}>
				{this.returnAvatarItemsIfHungry()}
			</ImageBackground>
			// return <Image style = {{width: 200, height: 200}} resizeMode='contain' source={require('../../assets/avatarImages/hungry_thirsty.jpeg')}/>
		}
	}

	returnAvatarItemsIfHappy = () => {
		if(this.props.storeItems.owned.length === 1) {
			let tempItem = this.props.storeItems.owned[0]
			if(tempItem === 'tophat' || tempItem === 'लंबा टोप') {
				return (<Image
					style={{width: 200, height: 200}}
					resizeMode="contain"
					source={require('../../assets/avatarImages/happy_lab_items/tophatOverlay.png')}
				/>)
			} else if(tempItem === 'mustache' || tempItem === 'मूंछ') {
				return (<Image
					style={{width: 200, height: 200}}
					resizeMode="contain"
					source={require('../../assets/avatarImages/happy_lab_items/mustacheOverlay.png')}
				/>)
			} else if(tempItem === 'glasses' || tempItem === 'चश्मा') {
				return (<Image
					style={{width: 200, height: 200}}
					resizeMode="contain"
					source={require('../../assets/avatarImages/happy_lab_items/glassesOverlay.png')}
				/>)
			}
		} else if(this.props.storeItems.owned.length === 2) {
			if((this.props.storeItems.owned.indexOf('tophat') !== -1 || this.props.storeItems.owned.indexOf('लंबा टोप') !== -1) &&
				(this.props.storeItems.owned.indexOf('glasses') !== -1 || this.props.storeItems.owned.indexOf('चश्मा') !== -1)) {
				return (<Image
					style={{width: 200, height: 200}}
					resizeMode="contain"
					source={require('../../assets/avatarImages/happy_lab_items/tophatAndGlasses.png')}
				/>)
			} else if((this.props.storeItems.owned.indexOf('tophat') !== -1 || this.props.storeItems.owned.indexOf('लंबा टोप') !== -1) &&
				(this.props.storeItems.owned.indexOf('mustache') !== -1 || this.props.storeItems.owned.indexOf('मूंछ') !== -1)) {
				return (<Image
					style={{width: 200, height: 200}}
					resizeMode="contain"
					source={require('../../assets/avatarImages/happy_lab_items/tophatAndMustache.png')}
				/>)
			} else if((this.props.storeItems.owned.indexOf('glasses') !== -1 || this.props.storeItems.owned.indexOf('चश्मा') !== -1) &&
				(this.props.storeItems.owned.indexOf('mustache') !== -1 || this.props.storeItems.owned.indexOf('मूंछ') !== -1)) {
				return (<Image
					style={{width: 200, height: 200}}
					resizeMode="contain"
					source={require('../../assets/avatarImages/happy_lab_items/glassesAndMustache.png')}
				/>)
			}
		} else if(this.props.storeItems.owned.length === 3) {
			return (<Image
				style={{width: 200, height: 200}}
				resizeMode="contain"
				source={require('../../assets/avatarImages/happy_lab_items/tophatAndMustacheAndGlasses.png')}
			/>)
		}
	}

	returnAvatarItemsIfHungry = () => {
		if(this.props.storeItems.owned.length === 1) {
			let tempItem = this.props.storeItems.owned[0]
			if(tempItem === 'tophat' || tempItem === 'लंबा टोप') {
				return (<Image
					style={{width: 200, height: 200}}
					resizeMode="contain"
					source={require('../../assets/avatarImages/hungry_thirsty_items/tophatOverlay.png')}
				/>)
			} else if(tempItem === 'mustache' || tempItem === 'मूंछ') {
				return (<Image
					style={{width: 200, height: 200}}
					resizeMode="contain"
					source={require('../../assets/avatarImages/hungry_thirsty_items/mustacheOverlay.png')}
				/>)
			} else if(tempItem === 'glasses' || tempItem === 'चश्मा') {
				return (<Image
					style={{width: 200, height: 200}}
					resizeMode="contain"
					source={require('../../assets/avatarImages/hungry_thirsty_items/glassesOverlay.png')}
				/>)
			}
		} else if(this.props.storeItems.owned.length === 2) {
				if((this.props.storeItems.owned.indexOf('tophat') !== -1 || this.props.storeItems.owned.indexOf('लंबा टोप') !== -1) &&
					(this.props.storeItems.owned.indexOf('glasses') !== -1 || this.props.storeItems.owned.indexOf('चश्मा') !== -1)) {
					return (<Image
						style={{width: 200, height: 200}}
						resizeMode="contain"
						source={require('../../assets/avatarImages/hungry_thirsty_items/tophatAndGlasses.png')}
					/>)
				} else if((this.props.storeItems.owned.indexOf('tophat') !== -1 || this.props.storeItems.owned.indexOf('लंबा टोप') !== -1) &&
					(this.props.storeItems.owned.indexOf('mustache') !== -1 || this.props.storeItems.owned.indexOf('मूंछ') !== -1)) {
					return (<Image
						style={{width: 200, height: 200}}
						resizeMode="contain"
						source={require('../../assets/avatarImages/hungry_thirsty_items/tophatAndMustache.png')}
					/>)
				} else if((this.props.storeItems.owned.indexOf('glasses') !== -1 || this.props.storeItems.owned.indexOf('चश्मा') !== -1) &&
					(this.props.storeItems.owned.indexOf('mustache') !== -1 || this.props.storeItems.owned.indexOf('मूंछ') !== -1)) {
					return (<Image
						style={{width: 200, height: 200}}
						resizeMode="contain"
						source={require('../../assets/avatarImages/hungry_thirsty_items/glassesAndMustache.png')}
					/>)
				}
		} else if(this.props.storeItems.owned.length === 3) {
			return (<Image
				style={{width: 200, height: 200}}
				resizeMode="contain"
				source={require('../../assets/avatarImages/hungry_thirsty_items/tophatAndMustacheAndGlasses.png')}
			/>)
		}
	}

	render() {
		this.lang = this.props.userSettings.language;
		let hungerTranslation = this.lang === "en" ? "Hunger: " : "भूख: ";
		let thirstTranslation = this.lang === "en" ? "Thirst: " : "प्यास: ";
		let exerciseTranslation = this.lang === "en" ? "Exercise: " : "व्यायाम: ";

		return (
			<Surface style = {{elevation: 3, padding: 10, marginTop: 25}}>
				<View style={{justifyContent: 'center', alignItems: 'center'}}>
					{this.returnPetImage()}
				</View>
				{this.returnAvatarMessage()}
				{this.returnAvatarExerciseMessage()}
				<Text>&nbsp;</Text>
				<Chip style = {{backgroundColor: '#5EDC70'}} icon = "food-apple">{hungerTranslation}{this.props.avatar.hunger}</Chip>
				<Text/>
				<Chip style = {{backgroundColor: '#66BAEF'}} icon = "cup-water">{thirstTranslation}{this.props.avatar.thirst}</Chip>
				<Text/>
				<Chip style = {{backgroundColor: '#F58792'}} icon = "run">{exerciseTranslation}{this.props.avatar.exercise}</Chip>
				<Text/>
				{this.returnUserWorkoutMessage()}
			</Surface>
		);
	}

	async componentDidMount(){
		avatar = this.props.avatar;
		updateAvatarState(avatar);
		//now update every 10 seconds
		this.props.avatar.avatarUpdateFunction = setInterval(function() {updateAvatarState(avatar)}, 10000);

		//get amount of time spent working out today.
		let workoutDBService = new WorkoutsDatabase();
		workoutDBService.getWorkoutsFromToday(this.props.ui);
		
		this.props.userSettings.language = await AsyncStorage.getItem("language");
	}
	componentWillUnmount(){
		clearInterval(this.props.avatar.avatarUpdateFunction);
	}
}

export default inject('avatar', 'ui', 'userSettings', 'storeItems')(observer(Avatar));
