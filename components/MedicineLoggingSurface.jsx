/**
 * @name MedicineLoggingSurface
 * @author Andrew Case
 *
 * @overview Medicine view that appears in accordian on Logging Page.
 *
 * @example
 * Used in LoggingPage
 * <MedicineLoggingSurface/>
 */

import React from 'react';
import { Text } from 'react-native';
import { Surface, Button, Subheading, Chip, Divider } from 'react-native-paper';
import { observer, inject } from 'mobx-react';
import MedicineLoggingDialog from './MedicineLoggingDialog';
import MedicineDatabase from '../utils/medicineDB';
import * as RootNavigation from "../state/RootNavigation";

const MedicineLoggingSurface = class MedicineLoggingSurface extends React.Component {
	lang;
	openDialog = () => {
		this.props.ui.medicineDialogOpen = true;
	};

	returnMedicationTakenToday = () => {
		let medicines = [];
		if (this.props.ui.medicineToday === null) {
			//no medication today...
			return <Text>{this.lang === 'en' ? 'None' : 'कोई नहीं'}</Text>;
		}

		for (let i = 0; i < this.props.ui.medicineToday._array.length; i++) {
			let currentMedicine = this.props.ui.medicineToday._array[i];
			let textToShow;
			if (this.lang === 'en') {
				let englishMedicine;
				switch (currentMedicine.dosage) {
					case 'OnceDaily':
						englishMedicine = 'Once Daily';
						break;
					case 'TwiceDaily':
						englishMedicine = 'Twice Daily';
						break;
					case 'ThreeDaily':
						englishMedicine = 'Three Times Daily';
						break;
					case 'OnceWeekly':
						englishMedicine = 'Once Weekly';
						break;
					case 'TwiceWeekly':
						englishMedicine = 'Twice Weekly';
						break;
					default:
						englishMedicine = 'Other';
				}
				textToShow = currentMedicine.name + ", Dosage: " + englishMedicine;
			} else {
				let hindiMedicine;
				switch (currentMedicine.dosage) {
					case 'OnceDaily':
						hindiMedicine = 'एक बार रोज़';
						break;
					case 'TwiceDaily':
						hindiMedicine = 'दिन में दो बार';
						break;
					case 'ThreeDaily':
						hindiMedicine = 'तीन बार दैनिक';
						break;
					case 'OnceWeekly':
						hindiMedicine = 'एक बार साप्ताहिक';
						break;
					case 'TwiceWeekly':
						hindiMedicine = 'सप्ताह में दो बार';
						break;
					default:
						hindiMedicine = 'अन्य';
				}
				textToShow = hindiMedicine + ', खुराक:' + hindiMedicine;
			}
			let icon = 'pill';

			medicines.push(
				<Chip key={i} icon={icon}>
					{textToShow}
				</Chip>
			);
			//push some spacing
			medicines.push(<Text key={i + 100} />);
		}
		return medicines;
	};

	returnNumberOfMedicationTakenToday = () => {
		let takenToday = 0;
		if(this.props.ui.lengthOfMedicinesToday !== null) {
			for(let i = 0; i < this.props.ui.lengthOfMedicinesToday; i++) {
				takenToday++;
			}
		}
        return <Text style = {{fontWeight: "bold"}}>{takenToday}</Text>
    }

	render() {
		this.lang = this.props.userSettings.language;
		return (
			<Surface style={{ alignItems: 'center', elevation: 3, padding: 10, marginTop: 10 }}>
				<Subheading>{this.lang === 'en' ? 'Medicine Logging' : 'दवा लॉगिंग'}</Subheading>
				<Text>
					{this.lang === 'en'
						? 'Get rewards for taking medication!'
						: 'दवा लेने के लिए पुरस्कार प्राप्त करें!'}
				</Text>
				<Text />

				<Button uppercase={false} onPress={this.openDialog} mode="contained">
					{this.lang === 'en' ? 'Enter Medicine' : 'दवा दर्ज करें'}
				</Button>
				<MedicineLoggingDialog/>

				<Text />
				<Text>{this.lang === 'en' ? 'Medicine taken today:' : 'आज ली गई दवा:'}</Text>
				{this.returnMedicationTakenToday()}
				<Divider style = {{width: 500}}/>
				<Text>{this.lang === "en" ? "Total Amount of Medication Taken Today:" : "आज ली गई दवा की कुल राशि:"} {this.returnNumberOfMedicationTakenToday()}</Text>
				<Text/>
                <Divider style = {{ width: 500 }}/>
                <Button
                    uppercase = {false}
                    onPress = {() => RootNavigation.navigate(this.lang === "en" ? "Medicine History" : "दवा का इतिहास")}
                >
                    {this.lang === "en" ? "View Medicine Logging History" : "चिकित्सा लॉगिंग इतिहास देखें"}
                </Button>
			</Surface>
		);
	}

	componentDidMount() {
		let medicineDBService = new MedicineDatabase();
		medicineDBService.initDB();
		medicineDBService.getMedicineFromToday(this.props.ui);
	}
};

export default inject('ui', 'userSettings')(observer(MedicineLoggingSurface));
