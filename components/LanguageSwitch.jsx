/**
 * @name LanguageSwitch
 * @author Andrew Case & Zac Steudel
 *
 * @overview Switch in the settings page that toggles between Hindi & English.
 *
 */

import React from 'react';
import { inject, observer } from 'mobx-react';
import { Text, View, AsyncStorage, Switch } from 'react-native';

const LanguageSwitch = class LanguageSwitch extends React.Component {
	handleChange = value => {
		this.props.userSettings.language === 'en'
			? (this.props.userSettings.language = 'hi')
			: (this.props.userSettings.language = 'en');

		//update the language setting in AsyncStorage
		AsyncStorage.setItem('language', this.props.userSettings.language);
	};

    render(){
        return(
            <View style = {{alignItems: 'center', flexDirection: "row", justifyContent: "center"}}>
                <Text>Hindi (हिन्दी)</Text>
                <Switch 
                    value={this.props.userSettings.language === "en"}
                    onValueChange={this.handleChange}
                />
                <Text>English</Text>
            </View>
        )
    }

	async componentDidMount() {
		this.props.userSettings.language = await AsyncStorage.getItem('language');
	}
};

export default inject('ui', 'userSettings')(observer(LanguageSwitch));
